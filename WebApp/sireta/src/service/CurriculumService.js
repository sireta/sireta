import api from './api'

class CurriculumService{
    getCurriculumByPersonId(id){
        return api.get('/curriculum/person/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    create(obj){
        return api.post('/curriculum/save', obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    downloadPDF(id){
        return api.get('/generateCV/'+id, {responseType: 'blob' ,headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new CurriculumService();
