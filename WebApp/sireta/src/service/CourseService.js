import api from './api'

class CourseService{
    create(obj){
        return api.post('/course/save',obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getCoursesByCurriculumId(id){
        return api.get('/course/getCoursesByCurriculum/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    delete(id){
        return api.delete('/course/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new CourseService();