import api from './api'

class VacantService{
    create(obj){
        return api.post('/vacants/save',obj,{headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getById(id){
        return api.get('/vacants/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    delete(id){
        return api.delete('/vacants/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getVacantsByFilter(username, filter){
        return api.get('/filterVacants/'+username+'/'+filter, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    updateStatus(status, vacante){
        /*status = status.replace(" ", "*");
        const token =  'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token;
        console.log(token)*/
        return api.post('/updateStatus/'+status+'/'+vacante, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new VacantService();