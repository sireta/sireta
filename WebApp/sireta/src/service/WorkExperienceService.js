import api from './api'

class WorkExperienceService{
    create(obj){
        return api.post('/workExperiences',obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getWorkExperienceByCurriculumId(id){
        return api.get('/workExperiences/getWorkExperienceByCurriculum/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    delete(id){
        return api.delete('/workExperiences/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new WorkExperienceService();