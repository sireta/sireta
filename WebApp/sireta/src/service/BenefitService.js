import api from './api'

class BenefitService{
    create(obj){
        return api.post('/beneficios/save',obj,{headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getByIdVacant(id){
        return api.get('/beneficios/byVacantId/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    deleteAllByVacantId(id){
        return api.delete('/beneficios/deleteAllByVacantId/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new BenefitService();