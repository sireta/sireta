import api from './api'

class CorreoService{
    sendEmail(correo, encabezado, mensaje){
        return api.post('/correo/'+correo+'/'+encabezado+'/'+mensaje+'', {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    recoverPassword(correo){
        return api.post('/correo/'+correo)
    }
}

export default new CorreoService();