import api from './api'

class AbilityService{
    create(obj){
        return api.post('/ability',obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getAbilitiesByCurriculumId(id){
        return api.get('/ability/getAbilitiesByCurriculum/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    delete(id){
        return api.delete('/ability/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new AbilityService();