import api from './api'

class SalaryService{
    create(obj){
        return api.post('/salaries/save',obj,{headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    delete(id){
        return api.delete('/salaries/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}})
    }
}

export default new SalaryService();