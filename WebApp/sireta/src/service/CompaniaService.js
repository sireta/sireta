import api from './api'

class CompaniaService{
    create(img){
        return api.post('/company/save',img)
    }
    create2(img){
        return api.post('/company/save2',img)
    }
    findByName(name){
        return api.get('/company/findByName/'+name)
    }
    deleteByname(name){
        return api.delete('/company/deleteByName/'+name)
    }
    update(id, img){
        return api.put('/company/update/'+id, img ,{headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}})
    }
    update2(id, img){
        return api.put('/company/update2/'+id, img ,{headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}})
    }
    findByNameExcludeThis(id,name){
        return api.get('/company/findByNameExcludeThis/'+id+'/'+name ,{headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}})
    }
}

export default new CompaniaService();
