import api from './api'

class ApplicantService{
    
    getById(id){
        return api.get('/applicant/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    update(obj){
        return api.put('/applicants',obj , {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    create(obj){
        return api.post('/applicants',obj)
    }
    delete(id){
        return api.delete('/applicants/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}})
    }
    getByIdPerson(id){
        return api.get('/applicant/user/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getApplicantByIdPerson(id){
        return api.get('/applicant/idperson/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getByPerson(id){
        return api.get('/applicants/user/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getFavoritesVacants(username){
        return api.get('/favorities/getFavoritesVacants/'+username, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getAllContacts(){
        return api.get('/persons/getAllPersons', {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getContacts(username){
        return api.get('/contact/getContacts/' + username, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    addContact(obj){
        return api.post('/contact/save', obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    deleteContact(id){
        return api.delete('/contact/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getVacantsByState(username){
        return api.get('/getVacants/'+username, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getBenefitsByVacant(username){
        return api.get('/benefitsByVacant/'+username, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new ApplicantService();
