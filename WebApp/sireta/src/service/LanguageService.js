import api from './api'

class LanguageService{
    create(obj){
        return api.post('/lenguaje/save',obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getCurriculumByPersonId(id){
        return api.get('/lenguajes/getAllByCurriculum/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    delete(id){
        return api.delete('/lenguaje/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new LanguageService();