import api from './api'

class UsuarioService{
    login(obj){//Crea el token. login.vue
        return api.post('/login',obj);
    }//Obtiene el rol del usuario por medio de su correo electrónico. App.vue
    getRole(username){
        return api.get('/persons/rol/'+username,{headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new UsuarioService();