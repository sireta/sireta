import api from './api'

class PersonaService{
    
    getById(id){
        return api.get('/persons/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    update(id,obj){
        return api.put('/persons/update/'+id, obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    create(obj){
        return api.post('/persons',obj)
    }
    delete(id){
        return api.delete('/persons/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}})
    }
    getByUsername(username){
        return api.get('/persons/user/'+username, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    updatePassword(id,obj){
        return api.put('/persons/updatePassword/'+id, obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    
}

export default new PersonaService();
