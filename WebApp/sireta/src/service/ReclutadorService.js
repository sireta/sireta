import api from './api'

class ReclutadorService{

    create(obj){
        return api.post('/recruiters',obj)
    }
    getByVacant(id) {
        return api.get('/ApplicantsByVacant/' + id);
    } 
    getApplicants() {
        return api.get('/applicants', {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getByIdPerson(id){
        return api.get('/recruiter/user/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});

    }
    getApplicantsByRecruiter(username){
        return api.get('/applicants/getApplicantsByRecruiter/'+username, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getVacantsByRecruiter(username){
        return api.get('/getMyVacants/'+username, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getBenefitsByRecruiter(username){
        return api.get('/beneficios/'+username, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new ReclutadorService();
