import api from './api'

class EducationService{
    create(obj){
        return api.post('/education/save',obj, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    getEducationsByCurriculumId(id){
        return api.get('/education/getEducationsByCurriculum/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
    delete(id){
        return api.delete('/education/'+id, {headers:{"Authorization": 'Bearer ' + (JSON.parse(localStorage.getItem('user'))).token}});
    }
}

export default new EducationService();