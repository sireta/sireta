import Vue from "vue";
import Router from "vue-router";

Vue.use(Router);

export default new Router({
    mode: "history",
    routes:[
        {
            path: "/",
            name: "inicio",
            component: () => import("./components/inicio/inicio")
        },
        {
            path: "/login",
            name: "login",
            component: () => import("./components/inicio/login")
        },
        {
            path: "/user/perfil",
            name: "miPerfil",
            component: () => import("./components/user/perfil")
        },
        {
            path: "/verPostulantes",
            name: "verPostulantes",
            component: () => import("./components/recruiter/verPostulantes")
        },
        {
            path: "/registro/reclutador",
            name: "reclutador",
            component: () => import("./components/registro/reclutador")
        },
        {
            path: "/registro/aplicante",
            name: "aplicante",
            component: () => import("./components/registro/aplicante")
        },
        {
            path: "/vacante/vacantes",
            name: "vacantes",
            component: () => import("./components/vacante/vacantes")
        },
        {
            path: "/vacante/agregar",
            name: "agregarVacante",
            component: () => import("./components/vacante/agregar")
        },
        {
            path: "/vacante/editar/:id",
            name: "editarVacante",
            component: () => import("./components/vacante/editar")
        },
        {
            path: "/favoritos",
            name: "favoritos",
            component: () => import("./components/user/favoritos")
        },
        {
            path: "/listaContactos",
            name: "listaContactos",
            component: () => import("./components/contactos/listaContactos")
        },
        {
            path: "/contactos",
            name: "contactos",
            component: () => import("./components/user/contactos")
        },
        {
            path: "/curriculum",
            name: "curriculum",
            component: () => import("./components/user/curriculum")
        },
        {
            path: "/vacante/verVacantes",
            name: "verVacantes",
            component: () => import("./components/vacante/verVacantes")
        }
    ]
});