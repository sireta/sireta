-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: talenting
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `ability`
--

DROP TABLE IF EXISTS `ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `ability` (
  `idAbility` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`idAbility`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `ability`
--

LOCK TABLES `ability` WRITE;
/*!40000 ALTER TABLE `ability` DISABLE KEYS */;
/*!40000 ALTER TABLE `ability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicant`
--

DROP TABLE IF EXISTS `applicant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `applicant` (
  `idApplicant` bigint NOT NULL AUTO_INCREMENT,
  `state` varchar(20) NOT NULL,
  `person_idPerson` bigint NOT NULL,
  PRIMARY KEY (`idApplicant`),
  KEY `fk_Applicant_Person1_idx` (`person_idPerson`),
  CONSTRAINT `fk_Applicant_Person1` FOREIGN KEY (`person_idPerson`) REFERENCES `person` (`idPerson`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicant`
--

LOCK TABLES `applicant` WRITE;
/*!40000 ALTER TABLE `applicant` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `authorities`
--

DROP TABLE IF EXISTS `authorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `authorities` (
  `username` varchar(100) NOT NULL,
  `authority` varchar(15) NOT NULL,
  PRIMARY KEY (`username`),
  KEY `fk_authorities_users1_idx` (`username`),
  CONSTRAINT `fk_authorities_users1` FOREIGN KEY (`username`) REFERENCES `users` (`username`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `authorities`
--

LOCK TABLES `authorities` WRITE;
/*!40000 ALTER TABLE `authorities` DISABLE KEYS */;
/*!40000 ALTER TABLE `authorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `benefit`
--

DROP TABLE IF EXISTS `benefit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `benefit` (
  `idbenefit` bigint NOT NULL AUTO_INCREMENT,
  `benefit` varchar(50) NOT NULL,
  PRIMARY KEY (`idbenefit`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `benefit`
--

LOCK TABLES `benefit` WRITE;
/*!40000 ALTER TABLE `benefit` DISABLE KEYS */;
/*!40000 ALTER TABLE `benefit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certification`
--

DROP TABLE IF EXISTS `certification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `certification` (
  `idCertification` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `obtainedDate` date NOT NULL,
  `expirationDate` date DEFAULT NULL,
  PRIMARY KEY (`idCertification`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certification`
--

LOCK TABLES `certification` WRITE;
/*!40000 ALTER TABLE `certification` DISABLE KEYS */;
/*!40000 ALTER TABLE `certification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `company`
--

DROP TABLE IF EXISTS `company`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `company` (
  `idCompany` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(60) NOT NULL,
  `state` varchar(20) NOT NULL,
  `logo` blob,
  PRIMARY KEY (`idCompany`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `company`
--

LOCK TABLES `company` WRITE;
/*!40000 ALTER TABLE `company` DISABLE KEYS */;
/*!40000 ALTER TABLE `company` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contac`
--

DROP TABLE IF EXISTS `contac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contac` (
  `idContac` bigint NOT NULL AUTO_INCREMENT,
  `person_Main` bigint NOT NULL,
  `person_Friend` bigint NOT NULL,
  PRIMARY KEY (`idContac`),
  KEY `fk_Contac_Person1_idx` (`person_Main`),
  KEY `fk_Contac_Person2_idx` (`person_Friend`),
  CONSTRAINT `fk_Contac_Person1` FOREIGN KEY (`person_Main`) REFERENCES `person` (`idPerson`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Contac_Person2` FOREIGN KEY (`person_Friend`) REFERENCES `person` (`idPerson`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contac`
--

LOCK TABLES `contac` WRITE;
/*!40000 ALTER TABLE `contac` DISABLE KEYS */;
/*!40000 ALTER TABLE `contac` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `course`
--

DROP TABLE IF EXISTS `course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `course` (
  `idCourse` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `hoursNumber` int NOT NULL,
  `issuingInstitute` varchar(45) NOT NULL,
  `realizationDate` date NOT NULL,
  PRIMARY KEY (`idCourse`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `course`
--

LOCK TABLES `course` WRITE;
/*!40000 ALTER TABLE `course` DISABLE KEYS */;
/*!40000 ALTER TABLE `course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculumgeneral`
--

DROP TABLE IF EXISTS `curriculumgeneral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curriculumgeneral` (
  `idCurriculumGeneral` bigint NOT NULL,
  `title` varchar(45) NOT NULL,
  `applicant_idApplicant` bigint NOT NULL,
  PRIMARY KEY (`idCurriculumGeneral`),
  KEY `fk_CurriculumGeneral_Applicant1_idx` (`applicant_idApplicant`),
  CONSTRAINT `fk_CurriculumGeneral_Applicant1` FOREIGN KEY (`applicant_idApplicant`) REFERENCES `applicant` (`idApplicant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculumgeneral`
--

LOCK TABLES `curriculumgeneral` WRITE;
/*!40000 ALTER TABLE `curriculumgeneral` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculumgeneral` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculumgeneral_has_ability`
--

DROP TABLE IF EXISTS `curriculumgeneral_has_ability`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curriculumgeneral_has_ability` (
  `curriculumGeneral_idCurriculumGeneral` bigint NOT NULL,
  `ability_idAbility` bigint NOT NULL,
  PRIMARY KEY (`curriculumGeneral_idCurriculumGeneral`,`ability_idAbility`),
  KEY `fk_curriculumGeneral_has_ability_ability1_idx` (`ability_idAbility`),
  KEY `fk_curriculumGeneral_has_ability_curriculumGeneral1_idx` (`curriculumGeneral_idCurriculumGeneral`),
  CONSTRAINT `fk_curriculumGeneral_has_ability_ability1` FOREIGN KEY (`ability_idAbility`) REFERENCES `ability` (`idAbility`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_curriculumGeneral_has_ability_curriculumGeneral1` FOREIGN KEY (`curriculumGeneral_idCurriculumGeneral`) REFERENCES `curriculumgeneral` (`idCurriculumGeneral`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculumgeneral_has_ability`
--

LOCK TABLES `curriculumgeneral_has_ability` WRITE;
/*!40000 ALTER TABLE `curriculumgeneral_has_ability` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculumgeneral_has_ability` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculumgeneral_has_certification`
--

DROP TABLE IF EXISTS `curriculumgeneral_has_certification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curriculumgeneral_has_certification` (
  `certification_idCertification` bigint NOT NULL,
  `curriculumGeneral_idCurriculumGeneral` bigint NOT NULL,
  PRIMARY KEY (`certification_idCertification`,`curriculumGeneral_idCurriculumGeneral`),
  KEY `fk_certification_has_curriculumGeneral_curriculumGeneral1_idx` (`curriculumGeneral_idCurriculumGeneral`),
  KEY `fk_certification_has_curriculumGeneral_certification1_idx` (`certification_idCertification`),
  CONSTRAINT `fk_certification_has_curriculumGeneral_certification1` FOREIGN KEY (`certification_idCertification`) REFERENCES `certification` (`idCertification`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_certification_has_curriculumGeneral_curriculumGeneral1` FOREIGN KEY (`curriculumGeneral_idCurriculumGeneral`) REFERENCES `curriculumgeneral` (`idCurriculumGeneral`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculumgeneral_has_certification`
--

LOCK TABLES `curriculumgeneral_has_certification` WRITE;
/*!40000 ALTER TABLE `curriculumgeneral_has_certification` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculumgeneral_has_certification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculumgeneral_has_course`
--

DROP TABLE IF EXISTS `curriculumgeneral_has_course`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curriculumgeneral_has_course` (
  `curriculumGeneral_idCurriculumGeneral` bigint NOT NULL,
  `course_idCourse` bigint NOT NULL,
  PRIMARY KEY (`curriculumGeneral_idCurriculumGeneral`,`course_idCourse`),
  KEY `fk_curriculumGeneral_has_course_course1_idx` (`course_idCourse`),
  KEY `fk_curriculumGeneral_has_course_curriculumGeneral1_idx` (`curriculumGeneral_idCurriculumGeneral`),
  CONSTRAINT `fk_curriculumGeneral_has_course_course1` FOREIGN KEY (`course_idCourse`) REFERENCES `course` (`idCourse`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_curriculumGeneral_has_course_curriculumGeneral1` FOREIGN KEY (`curriculumGeneral_idCurriculumGeneral`) REFERENCES `curriculumgeneral` (`idCurriculumGeneral`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculumgeneral_has_course`
--

LOCK TABLES `curriculumgeneral_has_course` WRITE;
/*!40000 ALTER TABLE `curriculumgeneral_has_course` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculumgeneral_has_course` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculumgeneral_has_education`
--

DROP TABLE IF EXISTS `curriculumgeneral_has_education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curriculumgeneral_has_education` (
  `curriculumGeneral_idCurriculumGeneral` bigint NOT NULL,
  `education_idEducation` bigint NOT NULL,
  PRIMARY KEY (`curriculumGeneral_idCurriculumGeneral`,`education_idEducation`),
  KEY `fk_curriculumGeneral_has_education_education1_idx` (`education_idEducation`),
  KEY `fk_curriculumGeneral_has_education_curriculumGeneral1_idx` (`curriculumGeneral_idCurriculumGeneral`),
  CONSTRAINT `fk_curriculumGeneral_has_education_curriculumGeneral1` FOREIGN KEY (`curriculumGeneral_idCurriculumGeneral`) REFERENCES `curriculumgeneral` (`idCurriculumGeneral`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_curriculumGeneral_has_education_education1` FOREIGN KEY (`education_idEducation`) REFERENCES `education` (`idEducation`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculumgeneral_has_education`
--

LOCK TABLES `curriculumgeneral_has_education` WRITE;
/*!40000 ALTER TABLE `curriculumgeneral_has_education` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculumgeneral_has_education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculumgeneral_has_language`
--

DROP TABLE IF EXISTS `curriculumgeneral_has_language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curriculumgeneral_has_language` (
  `curriculumGeneral_idCurriculumGeneral` bigint NOT NULL,
  `language_idLanguage` bigint NOT NULL,
  PRIMARY KEY (`curriculumGeneral_idCurriculumGeneral`,`language_idLanguage`),
  KEY `fk_curriculumGeneral_has_language_language1_idx` (`language_idLanguage`),
  KEY `fk_curriculumGeneral_has_language_curriculumGeneral1_idx` (`curriculumGeneral_idCurriculumGeneral`),
  CONSTRAINT `fk_curriculumGeneral_has_language_curriculumGeneral1` FOREIGN KEY (`curriculumGeneral_idCurriculumGeneral`) REFERENCES `curriculumgeneral` (`idCurriculumGeneral`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_curriculumGeneral_has_language_language1` FOREIGN KEY (`language_idLanguage`) REFERENCES `language` (`idLanguage`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculumgeneral_has_language`
--

LOCK TABLES `curriculumgeneral_has_language` WRITE;
/*!40000 ALTER TABLE `curriculumgeneral_has_language` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculumgeneral_has_language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculumgeneral_has_workexperience`
--

DROP TABLE IF EXISTS `curriculumgeneral_has_workexperience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curriculumgeneral_has_workexperience` (
  `curriculumGeneral_idCurriculumGeneral` bigint NOT NULL,
  `workExperience_idWorkExperience` bigint NOT NULL,
  PRIMARY KEY (`curriculumGeneral_idCurriculumGeneral`,`workExperience_idWorkExperience`),
  KEY `fk_curriculumGeneral_has_workExperience_workExperience1_idx` (`workExperience_idWorkExperience`),
  KEY `fk_curriculumGeneral_has_workExperience_curriculumGeneral1_idx` (`curriculumGeneral_idCurriculumGeneral`),
  CONSTRAINT `fk_curriculumGeneral_has_workExperience_curriculumGeneral1` FOREIGN KEY (`curriculumGeneral_idCurriculumGeneral`) REFERENCES `curriculumgeneral` (`idCurriculumGeneral`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_curriculumGeneral_has_workExperience_workExperience1` FOREIGN KEY (`workExperience_idWorkExperience`) REFERENCES `workexperience` (`idWorkExperience`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculumgeneral_has_workexperience`
--

LOCK TABLES `curriculumgeneral_has_workexperience` WRITE;
/*!40000 ALTER TABLE `curriculumgeneral_has_workexperience` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculumgeneral_has_workexperience` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `education` (
  `idEducation` bigint NOT NULL AUTO_INCREMENT,
  `educationalDegree` varchar(45) NOT NULL,
  `career` varchar(50) NOT NULL,
  `instituteName` varchar(45) NOT NULL,
  `initialDate` date NOT NULL,
  `finalDatel` date NOT NULL,
  PRIMARY KEY (`idEducation`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favority`
--

DROP TABLE IF EXISTS `favority`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `favority` (
  `applicant_idApplicant` bigint NOT NULL,
  `vacant_idVacant` bigint NOT NULL,
  PRIMARY KEY (`applicant_idApplicant`,`vacant_idVacant`),
  KEY `fk_applicant_has_vacant_vacant1_idx` (`vacant_idVacant`),
  KEY `fk_applicant_has_vacant_applicant1_idx` (`applicant_idApplicant`),
  CONSTRAINT `fk_applicant_has_vacant_applicant1` FOREIGN KEY (`applicant_idApplicant`) REFERENCES `applicant` (`idApplicant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_applicant_has_vacant_vacant1` FOREIGN KEY (`vacant_idVacant`) REFERENCES `vacant` (`idVacant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favority`
--

LOCK TABLES `favority` WRITE;
/*!40000 ALTER TABLE `favority` DISABLE KEYS */;
/*!40000 ALTER TABLE `favority` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `language`
--

DROP TABLE IF EXISTS `language`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `language` (
  `idLanguage` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(20) NOT NULL,
  `level` varchar(10) NOT NULL,
  PRIMARY KEY (`idLanguage`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `language`
--

LOCK TABLES `language` WRITE;
/*!40000 ALTER TABLE `language` DISABLE KEYS */;
/*!40000 ALTER TABLE `language` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notification`
--

DROP TABLE IF EXISTS `notification`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notification` (
  `idNotification` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `menssage` varchar(45) NOT NULL,
  `isOpen` tinyint(1) NOT NULL,
  `date` datetime NOT NULL,
  `person_Sender` bigint NOT NULL,
  `person_Receiver` bigint NOT NULL,
  PRIMARY KEY (`idNotification`),
  KEY `fk_Notification_Person1_idx` (`person_Sender`),
  KEY `fk_Notification_Person2_idx` (`person_Receiver`),
  CONSTRAINT `fk_Notification_Person1` FOREIGN KEY (`person_Sender`) REFERENCES `person` (`idPerson`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Notification_Person2` FOREIGN KEY (`person_Receiver`) REFERENCES `person` (`idPerson`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notification`
--

LOCK TABLES `notification` WRITE;
/*!40000 ALTER TABLE `notification` DISABLE KEYS */;
/*!40000 ALTER TABLE `notification` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `person`
--

DROP TABLE IF EXISTS `person`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `person` (
  `idPerson` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `firstLastName` varchar(45) NOT NULL,
  `SecondLastName` varchar(45) DEFAULT NULL,
  `username` varchar(100) NOT NULL,
  `phoneNumber` varchar(45) NOT NULL,
  `birthDate` date NOT NULL,
  `enabled` tinyint NOT NULL,
  `password` varchar(100) NOT NULL,
  `rol` varchar(15) NOT NULL,
  PRIMARY KEY (`idPerson`),
  UNIQUE KEY `PhoneNumber_UNIQUE` (`phoneNumber`),
  UNIQUE KEY `Email_UNIQUE` (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `person`
--

LOCK TABLES `person` WRITE;
/*!40000 ALTER TABLE `person` DISABLE KEYS */;
/*!40000 ALTER TABLE `person` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recruiter`
--

DROP TABLE IF EXISTS `recruiter`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recruiter` (
  `idRecruiter` bigint NOT NULL AUTO_INCREMENT,
  `job` varchar(45) NOT NULL,
  `person_idPerson` bigint NOT NULL,
  `company_idCompany` bigint NOT NULL,
  PRIMARY KEY (`idRecruiter`),
  KEY `fk_Recruiter_Person_idx` (`person_idPerson`),
  KEY `fk_Recruiter_Company1_idx` (`company_idCompany`),
  CONSTRAINT `fk_Recruiter_Company1` FOREIGN KEY (`company_idCompany`) REFERENCES `company` (`idCompany`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_Recruiter_Person` FOREIGN KEY (`person_idPerson`) REFERENCES `person` (`idPerson`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recruiter`
--

LOCK TABLES `recruiter` WRITE;
/*!40000 ALTER TABLE `recruiter` DISABLE KEYS */;
/*!40000 ALTER TABLE `recruiter` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salary`
--

DROP TABLE IF EXISTS `salary`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `salary` (
  `idsalary` bigint NOT NULL AUTO_INCREMENT,
  `minimum` float NOT NULL,
  `maximum` float NOT NULL,
  `periodicity` varchar(10) NOT NULL,
  PRIMARY KEY (`idsalary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salary`
--

LOCK TABLES `salary` WRITE;
/*!40000 ALTER TABLE `salary` DISABLE KEYS */;
/*!40000 ALTER TABLE `salary` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `users`
--

DROP TABLE IF EXISTS `users`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `users` (
  `username` varchar(100) NOT NULL,
  `password` varchar(100) NOT NULL,
  `enabled` tinyint NOT NULL,
  PRIMARY KEY (`username`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `users`
--

LOCK TABLES `users` WRITE;
/*!40000 ALTER TABLE `users` DISABLE KEYS */;
/*!40000 ALTER TABLE `users` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacant`
--

DROP TABLE IF EXISTS `vacant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacant` (
  `idVacant` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `description` varchar(255) NOT NULL,
  `modality` varchar(10) NOT NULL,
  `type` varchar(15) NOT NULL,
  `initialDate` date NOT NULL,
  `validityPeriod` date NOT NULL,
  `salary_idsalary` bigint NOT NULL,
  `recruiter_idRecruiter` bigint NOT NULL,
  PRIMARY KEY (`idVacant`),
  KEY `fk_vacant_salary1_idx` (`salary_idsalary`),
  KEY `fk_vacant_recruiter1_idx` (`recruiter_idRecruiter`),
  CONSTRAINT `fk_vacant_recruiter1` FOREIGN KEY (`recruiter_idRecruiter`) REFERENCES `recruiter` (`idRecruiter`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vacant_salary1` FOREIGN KEY (`salary_idsalary`) REFERENCES `salary` (`idsalary`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacant`
--

LOCK TABLES `vacant` WRITE;
/*!40000 ALTER TABLE `vacant` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacant_has_benefit`
--

DROP TABLE IF EXISTS `vacant_has_benefit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacant_has_benefit` (
  `vacant_idVacant` bigint NOT NULL,
  `benefit_idbenefit` bigint NOT NULL,
  PRIMARY KEY (`vacant_idVacant`,`benefit_idbenefit`),
  KEY `fk_vacant_has_benefit_benefit1_idx` (`benefit_idbenefit`),
  KEY `fk_vacant_has_benefit_vacant1_idx` (`vacant_idVacant`),
  CONSTRAINT `fk_vacant_has_benefit_benefit1` FOREIGN KEY (`benefit_idbenefit`) REFERENCES `benefit` (`idbenefit`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vacant_has_benefit_vacant1` FOREIGN KEY (`vacant_idVacant`) REFERENCES `vacant` (`idVacant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacant_has_benefit`
--

LOCK TABLES `vacant_has_benefit` WRITE;
/*!40000 ALTER TABLE `vacant_has_benefit` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacant_has_benefit` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacantbyapplicant`
--

DROP TABLE IF EXISTS `vacantbyapplicant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacantbyapplicant` (
  `idvacantByApplicant` bigint NOT NULL AUTO_INCREMENT,
  `isOpenCV` tinyint NOT NULL,
  `curriculumPDF` blob NOT NULL,
  `Status` varchar(15) NOT NULL,
  `applicant_idApplicant` bigint NOT NULL,
  `vacant_idVacant` bigint NOT NULL,
  PRIMARY KEY (`idvacantByApplicant`,`applicant_idApplicant`),
  KEY `fk_vacantByApplicant_applicant1_idx` (`applicant_idApplicant`),
  KEY `fk_vacantByApplicant_vacant1_idx` (`vacant_idVacant`),
  CONSTRAINT `fk_vacantByApplicant_applicant1` FOREIGN KEY (`applicant_idApplicant`) REFERENCES `applicant` (`idApplicant`) ON DELETE CASCADE ON UPDATE CASCADE,
  CONSTRAINT `fk_vacantByApplicant_vacant1` FOREIGN KEY (`vacant_idVacant`) REFERENCES `vacant` (`idVacant`) ON DELETE CASCADE ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacantbyapplicant`
--

LOCK TABLES `vacantbyapplicant` WRITE;
/*!40000 ALTER TABLE `vacantbyapplicant` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacantbyapplicant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `workexperience`
--

DROP TABLE IF EXISTS `workexperience`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `workexperience` (
  `idWorkExperience` bigint NOT NULL AUTO_INCREMENT,
  `job` varchar(45) NOT NULL,
  `initialDate` date NOT NULL,
  `finalDate` date NOT NULL,
  `activities` varchar(255) NOT NULL,
  PRIMARY KEY (`idWorkExperience`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `workexperience`
--

LOCK TABLES `workexperience` WRITE;
/*!40000 ALTER TABLE `workexperience` DISABLE KEYS */;
/*!40000 ALTER TABLE `workexperience` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-06-26 20:42:20
