-- MySQL dump 10.13  Distrib 8.0.22, for Win64 (x86_64)
--
-- Host: 127.0.0.1    Database: sireta
-- ------------------------------------------------------
-- Server version	8.0.22

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `abilities`
--

DROP TABLE IF EXISTS `abilities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `abilities` (
  `id_ability` bigint NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `curriculum_idcurriculum` bigint DEFAULT NULL,
  PRIMARY KEY (`id_ability`),
  KEY `FKfk29gcpkkk2ugjj7p8jydrml1` (`curriculum_idcurriculum`),
  CONSTRAINT `FKfk29gcpkkk2ugjj7p8jydrml1` FOREIGN KEY (`curriculum_idcurriculum`) REFERENCES `curriculum` (`id_curriculum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `abilities`
--

LOCK TABLES `abilities` WRITE;
/*!40000 ALTER TABLE `abilities` DISABLE KEYS */;
/*!40000 ALTER TABLE `abilities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `activities`
--

DROP TABLE IF EXISTS `activities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `activities` (
  `id_activity` bigint NOT NULL AUTO_INCREMENT,
  `activity` varchar(60) NOT NULL,
  `work_experience_id_work_experience` bigint DEFAULT NULL,
  PRIMARY KEY (`id_activity`),
  KEY `FKdg4y3on6l45d7uejcvqupxbd6` (`work_experience_id_work_experience`),
  CONSTRAINT `FKdg4y3on6l45d7uejcvqupxbd6` FOREIGN KEY (`work_experience_id_work_experience`) REFERENCES `work_experiences` (`id_work_experience`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `activities`
--

LOCK TABLES `activities` WRITE;
/*!40000 ALTER TABLE `activities` DISABLE KEYS */;
/*!40000 ALTER TABLE `activities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `applicants`
--

DROP TABLE IF EXISTS `applicants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `applicants` (
  `id_applicant` bigint NOT NULL AUTO_INCREMENT,
  `state` varchar(20) NOT NULL,
  `persons_id_persons` bigint DEFAULT NULL,
  PRIMARY KEY (`id_applicant`),
  KEY `FKegjvwvuj7d20wuaf1erh65mc2` (`persons_id_persons`),
  CONSTRAINT `FKegjvwvuj7d20wuaf1erh65mc2` FOREIGN KEY (`persons_id_persons`) REFERENCES `persons` (`id_person`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `applicants`
--

LOCK TABLES `applicants` WRITE;
/*!40000 ALTER TABLE `applicants` DISABLE KEYS */;
/*!40000 ALTER TABLE `applicants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `benefits`
--

DROP TABLE IF EXISTS `benefits`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `benefits` (
  `id_benefit` bigint NOT NULL AUTO_INCREMENT,
  `benefit` varchar(50) NOT NULL,
  `vacant_id_vacant` bigint DEFAULT NULL,
  PRIMARY KEY (`id_benefit`),
  KEY `FKgyxtwm1r25givo1udgg765h5n` (`vacant_id_vacant`),
  CONSTRAINT `FKgyxtwm1r25givo1udgg765h5n` FOREIGN KEY (`vacant_id_vacant`) REFERENCES `vacants` (`id_vacant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `benefits`
--

LOCK TABLES `benefits` WRITE;
/*!40000 ALTER TABLE `benefits` DISABLE KEYS */;
/*!40000 ALTER TABLE `benefits` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `certifications`
--

DROP TABLE IF EXISTS `certifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `certifications` (
  `id_certification` bigint NOT NULL AUTO_INCREMENT,
  `expiration_date` date NOT NULL,
  `name` varchar(50) NOT NULL,
  `obtained_date` date NOT NULL,
  `curriculum_id_curriculum` bigint DEFAULT NULL,
  PRIMARY KEY (`id_certification`),
  KEY `FKejtywebbtydcj40bui37lrbrw` (`curriculum_id_curriculum`),
  CONSTRAINT `FKejtywebbtydcj40bui37lrbrw` FOREIGN KEY (`curriculum_id_curriculum`) REFERENCES `curriculum` (`id_curriculum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `certifications`
--

LOCK TABLES `certifications` WRITE;
/*!40000 ALTER TABLE `certifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `certifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `companies`
--

DROP TABLE IF EXISTS `companies`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `companies` (
  `id_company` bigint NOT NULL AUTO_INCREMENT,
  `logo` longblob,
  `name` varchar(60) NOT NULL,
  `state` varchar(20) NOT NULL,
  PRIMARY KEY (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `companies`
--

LOCK TABLES `companies` WRITE;
/*!40000 ALTER TABLE `companies` DISABLE KEYS */;
/*!40000 ALTER TABLE `companies` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `contacs`
--

DROP TABLE IF EXISTS `contacs`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `contacs` (
  `id_contac` bigint NOT NULL AUTO_INCREMENT,
  `person_friend` bigint DEFAULT NULL,
  `person_main` bigint DEFAULT NULL,
  PRIMARY KEY (`id_contac`),
  KEY `FKffamo43ba8y9nvruff0g6qkeu` (`person_friend`),
  KEY `FKouefam58t3qouy4p5qbfvia9n` (`person_main`),
  CONSTRAINT `FKffamo43ba8y9nvruff0g6qkeu` FOREIGN KEY (`person_friend`) REFERENCES `persons` (`id_person`),
  CONSTRAINT `FKouefam58t3qouy4p5qbfvia9n` FOREIGN KEY (`person_main`) REFERENCES `persons` (`id_person`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `contacs`
--

LOCK TABLES `contacs` WRITE;
/*!40000 ALTER TABLE `contacs` DISABLE KEYS */;
/*!40000 ALTER TABLE `contacs` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `courses`
--

DROP TABLE IF EXISTS `courses`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `courses` (
  `id_course` bigint NOT NULL AUTO_INCREMENT,
  `hours_number` int NOT NULL,
  `issuing_institute` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `realization_date` date DEFAULT NULL,
  `curriculum_id_curriculum` bigint DEFAULT NULL,
  PRIMARY KEY (`id_course`),
  KEY `FKknx7hwn860dxb419bk9t69wk6` (`curriculum_id_curriculum`),
  CONSTRAINT `FKknx7hwn860dxb419bk9t69wk6` FOREIGN KEY (`curriculum_id_curriculum`) REFERENCES `curriculum` (`id_curriculum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `courses`
--

LOCK TABLES `courses` WRITE;
/*!40000 ALTER TABLE `courses` DISABLE KEYS */;
/*!40000 ALTER TABLE `courses` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `curriculum`
--

DROP TABLE IF EXISTS `curriculum`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `curriculum` (
  `id_curriculum` bigint NOT NULL AUTO_INCREMENT,
  `title` varchar(45) NOT NULL,
  `applicant_id_applicant` bigint DEFAULT NULL,
  PRIMARY KEY (`id_curriculum`),
  KEY `FKsgvll9aqmutybhiqgjsq5gl9q` (`applicant_id_applicant`),
  CONSTRAINT `FKsgvll9aqmutybhiqgjsq5gl9q` FOREIGN KEY (`applicant_id_applicant`) REFERENCES `applicants` (`id_applicant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `curriculum`
--

LOCK TABLES `curriculum` WRITE;
/*!40000 ALTER TABLE `curriculum` DISABLE KEYS */;
/*!40000 ALTER TABLE `curriculum` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `education`
--

DROP TABLE IF EXISTS `education`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `education` (
  `id_education` bigint NOT NULL AUTO_INCREMENT,
  `career` varchar(50) NOT NULL,
  `educational_degree` varchar(45) NOT NULL,
  `final_date` date DEFAULT NULL,
  `initial_date` date DEFAULT NULL,
  `institute_name` varchar(45) NOT NULL,
  `curriculum_id_curriculum` bigint DEFAULT NULL,
  PRIMARY KEY (`id_education`),
  KEY `FKkit40pr84rr27j7xhstr03jxq` (`curriculum_id_curriculum`),
  CONSTRAINT `FKkit40pr84rr27j7xhstr03jxq` FOREIGN KEY (`curriculum_id_curriculum`) REFERENCES `curriculum` (`id_curriculum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `education`
--

LOCK TABLES `education` WRITE;
/*!40000 ALTER TABLE `education` DISABLE KEYS */;
/*!40000 ALTER TABLE `education` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `favorities`
--

DROP TABLE IF EXISTS `favorities`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `favorities` (
  `id_favority` bigint NOT NULL AUTO_INCREMENT,
  `applicant_id_applicant` bigint DEFAULT NULL,
  `vacant_id_vacant` bigint DEFAULT NULL,
  PRIMARY KEY (`id_favority`),
  KEY `FKn69ep62hr9h3xtjuqxqhe9egm` (`applicant_id_applicant`),
  KEY `FK73ql6pemcfmrkwlcdfwak0fja` (`vacant_id_vacant`),
  CONSTRAINT `FK73ql6pemcfmrkwlcdfwak0fja` FOREIGN KEY (`vacant_id_vacant`) REFERENCES `vacants` (`id_vacant`),
  CONSTRAINT `FKn69ep62hr9h3xtjuqxqhe9egm` FOREIGN KEY (`applicant_id_applicant`) REFERENCES `applicants` (`id_applicant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `favorities`
--

LOCK TABLES `favorities` WRITE;
/*!40000 ALTER TABLE `favorities` DISABLE KEYS */;
/*!40000 ALTER TABLE `favorities` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `languages`
--

DROP TABLE IF EXISTS `languages`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `languages` (
  `id_language` bigint NOT NULL AUTO_INCREMENT,
  `level` varchar(10) NOT NULL,
  `name` varchar(20) NOT NULL,
  `curriculum_id_curriculum` bigint DEFAULT NULL,
  PRIMARY KEY (`id_language`),
  KEY `FK6avohuihwaiub9dxv0yw0y5o8` (`curriculum_id_curriculum`),
  CONSTRAINT `FK6avohuihwaiub9dxv0yw0y5o8` FOREIGN KEY (`curriculum_id_curriculum`) REFERENCES `curriculum` (`id_curriculum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `languages`
--

LOCK TABLES `languages` WRITE;
/*!40000 ALTER TABLE `languages` DISABLE KEYS */;
/*!40000 ALTER TABLE `languages` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `notifications`
--

DROP TABLE IF EXISTS `notifications`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `notifications` (
  `id_notification` bigint NOT NULL AUTO_INCREMENT,
  `date` datetime(6) NOT NULL,
  `is_open` tinyint NOT NULL,
  `message` varchar(45) NOT NULL,
  `title` varchar(45) NOT NULL,
  `person_receiver` bigint DEFAULT NULL,
  `person_sender` bigint DEFAULT NULL,
  PRIMARY KEY (`id_notification`),
  KEY `FK6jfow1vta1ps4ymo7jpcp80f0` (`person_receiver`),
  KEY `FKnc8i9ynmoxset94rab09ngcgg` (`person_sender`),
  CONSTRAINT `FK6jfow1vta1ps4ymo7jpcp80f0` FOREIGN KEY (`person_receiver`) REFERENCES `persons` (`id_person`),
  CONSTRAINT `FKnc8i9ynmoxset94rab09ngcgg` FOREIGN KEY (`person_sender`) REFERENCES `persons` (`id_person`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `notifications`
--

LOCK TABLES `notifications` WRITE;
/*!40000 ALTER TABLE `notifications` DISABLE KEYS */;
/*!40000 ALTER TABLE `notifications` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `persons`
--

DROP TABLE IF EXISTS `persons`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `persons` (
  `id_person` bigint NOT NULL AUTO_INCREMENT,
  `second_last_name` varchar(45) DEFAULT NULL,
  `birth_date` date NOT NULL,
  `enabled` tinyint NOT NULL,
  `first_last_name` varchar(45) NOT NULL,
  `name` varchar(45) NOT NULL,
  `password` varchar(100) NOT NULL,
  `phone_number` varchar(45) NOT NULL,
  `username` varchar(100) NOT NULL,
  `rol_id_rol` int DEFAULT NULL,
  PRIMARY KEY (`id_person`),
  KEY `FK9vqchbtvsd6w6vxkhtfalyk22` (`rol_id_rol`),
  CONSTRAINT `FK9vqchbtvsd6w6vxkhtfalyk22` FOREIGN KEY (`rol_id_rol`) REFERENCES `rol` (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `persons`
--

LOCK TABLES `persons` WRITE;
/*!40000 ALTER TABLE `persons` DISABLE KEYS */;
INSERT INTO `persons` VALUES (1,'Baltazar','2001-05-02',1,'Gonzalez','Daniel','$2a$10$97c5uJhR.L8.nm0HUTHwSOXSrHzgDQSP2sXSHiZX/3HNRMrTindfi','7776023100','20193tn108@utez.edu.mx',1);
/*!40000 ALTER TABLE `persons` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `recruiters`
--

DROP TABLE IF EXISTS `recruiters`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `recruiters` (
  `id_recruiter` bigint NOT NULL AUTO_INCREMENT,
  `job` varchar(45) NOT NULL,
  `company_id_company` bigint DEFAULT NULL,
  `person_id_person` bigint DEFAULT NULL,
  PRIMARY KEY (`id_recruiter`),
  KEY `FKhs9bggawdkte8k6eug4xiag1m` (`company_id_company`),
  KEY `FKe43d5j35ykxpdmyndey9unrsl` (`person_id_person`),
  CONSTRAINT `FKe43d5j35ykxpdmyndey9unrsl` FOREIGN KEY (`person_id_person`) REFERENCES `persons` (`id_person`),
  CONSTRAINT `FKhs9bggawdkte8k6eug4xiag1m` FOREIGN KEY (`company_id_company`) REFERENCES `companies` (`id_company`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `recruiters`
--

LOCK TABLES `recruiters` WRITE;
/*!40000 ALTER TABLE `recruiters` DISABLE KEYS */;
/*!40000 ALTER TABLE `recruiters` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `rol`
--

DROP TABLE IF EXISTS `rol`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `rol` (
  `id_rol` int NOT NULL AUTO_INCREMENT,
  `nombre` varchar(21) DEFAULT NULL,
  PRIMARY KEY (`id_rol`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `rol`
--

LOCK TABLES `rol` WRITE;
/*!40000 ALTER TABLE `rol` DISABLE KEYS */;
INSERT INTO `rol` VALUES (1,'recruiter'),(2,'applicant');
/*!40000 ALTER TABLE `rol` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `salaries`
--

DROP TABLE IF EXISTS `salaries`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `salaries` (
  `id_salary` bigint NOT NULL AUTO_INCREMENT,
  `maximum` float NOT NULL,
  `minimum` float NOT NULL,
  `periodicity` varchar(10) NOT NULL,
  PRIMARY KEY (`id_salary`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `salaries`
--

LOCK TABLES `salaries` WRITE;
/*!40000 ALTER TABLE `salaries` DISABLE KEYS */;
/*!40000 ALTER TABLE `salaries` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacant_by_applicant`
--

DROP TABLE IF EXISTS `vacant_by_applicant`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacant_by_applicant` (
  `id_vacant_by_applicant` bigint NOT NULL AUTO_INCREMENT,
  `curriculumpdf` longblob,
  `is_opencv` tinyint NOT NULL,
  `status` varchar(15) NOT NULL,
  `applicant_id_applicant` bigint DEFAULT NULL,
  `vacant_id_vacant` bigint DEFAULT NULL,
  PRIMARY KEY (`id_vacant_by_applicant`),
  KEY `FKgt3hlrjsh0gm06oxhof6uegu7` (`applicant_id_applicant`),
  KEY `FKa3tfv1q5ekqri3xkw0hku5ovb` (`vacant_id_vacant`),
  CONSTRAINT `FKa3tfv1q5ekqri3xkw0hku5ovb` FOREIGN KEY (`vacant_id_vacant`) REFERENCES `vacants` (`id_vacant`),
  CONSTRAINT `FKgt3hlrjsh0gm06oxhof6uegu7` FOREIGN KEY (`applicant_id_applicant`) REFERENCES `applicants` (`id_applicant`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacant_by_applicant`
--

LOCK TABLES `vacant_by_applicant` WRITE;
/*!40000 ALTER TABLE `vacant_by_applicant` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacant_by_applicant` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `vacants`
--

DROP TABLE IF EXISTS `vacants`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `vacants` (
  `id_vacant` bigint NOT NULL AUTO_INCREMENT,
  `creation_date` datetime(6) DEFAULT NULL,
  `description` varchar(255) NOT NULL,
  `initial_date` date NOT NULL,
  `modality` varchar(10) NOT NULL,
  `title` varchar(45) NOT NULL,
  `type` varchar(15) NOT NULL,
  `validity_period` date NOT NULL,
  `recruiter_id_recruiter` bigint DEFAULT NULL,
  `salary_id_salary` bigint DEFAULT NULL,
  PRIMARY KEY (`id_vacant`),
  KEY `FK5bwoqykyxnh86jghwfoilc3uo` (`recruiter_id_recruiter`),
  KEY `FK3p3bxjgdv27rf9jloo84a6nfy` (`salary_id_salary`),
  CONSTRAINT `FK3p3bxjgdv27rf9jloo84a6nfy` FOREIGN KEY (`salary_id_salary`) REFERENCES `salaries` (`id_salary`),
  CONSTRAINT `FK5bwoqykyxnh86jghwfoilc3uo` FOREIGN KEY (`recruiter_id_recruiter`) REFERENCES `recruiters` (`id_recruiter`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `vacants`
--

LOCK TABLES `vacants` WRITE;
/*!40000 ALTER TABLE `vacants` DISABLE KEYS */;
/*!40000 ALTER TABLE `vacants` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `work_experiences`
--

DROP TABLE IF EXISTS `work_experiences`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `work_experiences` (
  `id_work_experience` bigint NOT NULL AUTO_INCREMENT,
  `final_date` date NOT NULL,
  `initial_date` date NOT NULL,
  `job` varchar(45) NOT NULL,
  `curriculum_id_curriculum` bigint DEFAULT NULL,
  PRIMARY KEY (`id_work_experience`),
  KEY `FKtgi4e3g14sjpipsy2irmsk5w` (`curriculum_id_curriculum`),
  CONSTRAINT `FKtgi4e3g14sjpipsy2irmsk5w` FOREIGN KEY (`curriculum_id_curriculum`) REFERENCES `curriculum` (`id_curriculum`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `work_experiences`
--

LOCK TABLES `work_experiences` WRITE;
/*!40000 ALTER TABLE `work_experiences` DISABLE KEYS */;
/*!40000 ALTER TABLE `work_experiences` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2022-07-25  8:40:34
