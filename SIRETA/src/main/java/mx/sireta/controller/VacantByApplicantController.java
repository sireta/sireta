package mx.sireta.controller;

import mx.sireta.entity.VacantByApplicant;
import mx.sireta.service.VacantByApplicantService;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/sireta")
public class VacantByApplicantController {

	private final Logger logger = LoggerFactory.getLogger(VacantByApplicantController.class);
	
    @Autowired
    private VacantByApplicantService vacantByApplicantService;

    @GetMapping("/vacantByApplicant")
    public List<VacantByApplicant> getVacantByApplicant(){
        return vacantByApplicantService.getAll();
    }

    @PostMapping("/vacantByApplicant")
    public VacantByApplicant save(@RequestBody VacantByApplicant vacantByApplicant) {
        return vacantByApplicantService.saveOrUpdate(vacantByApplicant);
    }

    @PutMapping("/vacantByApplicant")
    public VacantByApplicant update(@RequestBody VacantByApplicant vacantByApplicant) {
        return vacantByApplicantService.saveOrUpdate(vacantByApplicant);
    }

    @DeleteMapping("/vacantByApplicant/{id}")
    public void delete(@PathVariable("id") Long id) {
        vacantByApplicantService.remove(id);
    }

    @GetMapping("/vacantByApplicant/{id}")
    public void getVacantByApplicant(@PathVariable("id") Long id) {
        vacantByApplicantService.getVacantByApplicant(id);
    }
    
    @GetMapping("/ApplicantsByVacant/{id}")
    public void getApplicantsByVacant(@PathVariable("id") Long id) {
        vacantByApplicantService.getApplicantsByVacant(id);
    }
    
    @PostMapping("/updateStatus/{status}/{vacante}")
    public void updateStatus(@PathVariable("status") String status, @PathVariable("vacante") Long vacante) {
        logger.info("status" + status);
    	vacantByApplicantService.updateStatus(status,  vacante);
    }

}
