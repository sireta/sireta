package mx.sireta.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Benefits;
import mx.sireta.security.ErrorHandler;
import mx.sireta.service.BenefitsService;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class BenefitsController {

    @Autowired
    private BenefitsService benefitsService;

    @GetMapping("/beneficios")
    public List<Benefits> list(){
        return benefitsService.getAll();
    }

    @GetMapping("/beneficios{id}")
    public Benefits edit(@PathVariable("id") Long id){
        return benefitsService.getOne(id);
    }
    
    @PostMapping("/beneficios/save")
    public void save(@RequestBody Benefits benefit){
        benefitsService.saveOrUpdate(benefit);
    }

    @PutMapping("/beneficios/update")
    public void update(@RequestBody Benefits benefit){
        benefitsService.saveOrUpdate(benefit);
    }

    @DeleteMapping("/beneficios/{id}")
    public void delete(@PathVariable("id") Long id){
        benefitsService.remove(id);
    }

    @DeleteMapping("/beneficios/deleteAllByVacantId/{idVacant}")
    public void deleteAllByVacantId(@PathVariable("idVacant") Long idVacant){
	    benefitsService.deleteAllByVacantId(idVacant);
    }
    
    @GetMapping("/beneficios/{username}")
    public List<Benefits> getBenefitsByRecruiter(@PathVariable("username") String username){
        return benefitsService.getBenefitsByRecruiter(username);
    }

    @GetMapping("/beneficios/byVacantId/{idVacant}")
    public List<Benefits> getBenefitsByIdVacant(@PathVariable("idVacant") Long idVacant){
        return benefitsService.getBenefitsByIdVacant(idVacant);
    }
    
    @GetMapping("/benefitsByVacant/{username}")
    public List<Benefits> getBenefitsByVacant(@PathVariable("username") String username){
        return benefitsService.getBenefitsByVacant(username);
    }
}
