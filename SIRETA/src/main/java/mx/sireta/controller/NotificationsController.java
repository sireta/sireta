package mx.sireta.controller;

import java.util.List;

import mx.sireta.service.NotificationsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Notifications;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class NotificationsController {
	
	@Autowired
	private NotificationsService notificationService;

	@GetMapping("/notifications")
	public List<Notifications> list(){
		return notificationService.getAll();
	}

	@GetMapping("/notifications/{id}")
	public Notifications edit(@PathVariable("id") Long id){
		return notificationService.getOne(id);
	}

	@PostMapping("/notifications")
	public Notifications save(@RequestBody Notifications notification){
		return notificationService.saveOrUpdate(notification);
	}

	@PutMapping("/notifications/update")
	public void update(@RequestBody Notifications notification){
		notificationService.saveOrUpdate(notification);
	}

	@DeleteMapping("/notifications/{id}")
	public void delete(@PathVariable("id") Long id){
		notificationService.remove(id);
	}
	
}
