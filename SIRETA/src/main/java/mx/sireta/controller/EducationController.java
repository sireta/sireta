package mx.sireta.controller;

import java.util.List;

import mx.sireta.security.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Education;
import mx.sireta.service.EducationService;

import javax.validation.Valid;


@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class EducationController {

    @Autowired
	private EducationService educationService;

	@GetMapping("/education")
	public List<Education> getEducations(){
		return educationService.getAll();
	}
    
    @GetMapping("/education/{id}")
	public Education getEducations(@PathVariable("id") Long id){
		return educationService.getEducation(id);
	}
    
	@PostMapping("/education/save")
	public ResponseEntity<Object> save(@Valid @RequestBody Education education, BindingResult result) {
		if(result.hasErrors()){
			return new Validity().entity_IsValid(result);
		}
		try{
			educationService.saveOrUpdate(education);
		}catch (Exception e){
			return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, "Ok!");
	}

	@GetMapping("/education/getEducationsByCurriculum/{id}")
	public List<Education> getEducationsByCurriculum(@PathVariable("id") Long id){
		return educationService.getEducationsByCurriculum(id);
	}

	@PutMapping("/education/update")
	public Education update(@RequestBody Education education) {
		return educationService.saveOrUpdate(education);
	}
	
	@DeleteMapping("/education/{id}")
	public void delete(@PathVariable("id") Long id) {
		educationService.remove(id);
	}
}
