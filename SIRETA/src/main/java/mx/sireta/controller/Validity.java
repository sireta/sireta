package mx.sireta.controller;

import mx.sireta.security.ErrorHandler;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;

public class Validity {

    public ResponseEntity<Object> entity_IsValid(BindingResult result){
        String errors = "";
        for (ObjectError error : result.getAllErrors()) {
            errors = errors + error.getDefaultMessage() + "--";
        }
        return ErrorHandler.generateResponse("error", HttpStatus.OK, errors);
    }

}
