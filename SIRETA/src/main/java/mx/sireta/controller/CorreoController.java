package mx.sireta.controller;

import mx.sireta.service.CorreoService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/sireta")
public class CorreoController {

    @Autowired
    private CorreoService correoService;

    //{correo} -> A quien va dirigido
    //{encabezado} = Encabezado del correo
    //{mensaje} = Contenido del correo
    @PostMapping("/correo/{correo}/{encabezado}/{mensaje}")
    public void sendEmail(@PathVariable("correo") String correo, @PathVariable("encabezado") String encabezado, @PathVariable("mensaje") String mensaje){
        correoService.correo(correo,encabezado,mensaje);
    }

    @PostMapping("/correo/{correo}")
    public void recoverPassword(@PathVariable("correo") String correo){
        correoService.recoverPassword(correo);
    }

}
