package mx.sireta.controller;

import java.util.List;

import mx.sireta.security.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.sireta.entity.Abilities;
import mx.sireta.service.AbilitiesService;

import javax.validation.Valid;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class AbilitiesController {

    @Autowired
    private AbilitiesService abilitiesService;

    @GetMapping("/abilities")
    public List<Abilities> list(){
        return abilitiesService.getAll();
    }

    @GetMapping("/abilities/{id}")
    public Abilities edit(@PathVariable("id") Long id){
        return abilitiesService.getOne(id);
    }

    @GetMapping("/ability/getAbilitiesByCurriculum/{id}")
    public List<Abilities> getAbilitiesByPersonId(@PathVariable("id") Long id){
        return abilitiesService.getAbilitiesByPersonId(id);
    }

    @PostMapping("/ability")
    public ResponseEntity<Object> save(@Valid @RequestBody Abilities ability, BindingResult result){
        if (result.hasErrors()) {
            return new Validity().entity_IsValid(result);
        }
        try{
            abilitiesService.saveOrUpdate(ability);
        }catch (Exception e){
            return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
        }
        return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, "Ok!");
    }
    
    @PutMapping("/ability/update")
    public void update(@RequestBody Abilities ability){
        abilitiesService.saveOrUpdate(ability);
    }

    @DeleteMapping("/ability/{id}")
    public void delete(@PathVariable("id") Long id){
        abilitiesService.remove(id);
    }
}
