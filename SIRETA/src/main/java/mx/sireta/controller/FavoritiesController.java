package mx.sireta.controller;

import mx.sireta.entity.Favorities;
import mx.sireta.entity.Vacant;
import mx.sireta.service.FavoritiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class FavoritiesController {

    @Autowired
    private FavoritiesService favoritiesService;

    @GetMapping("/favorities")
    public List<Favorities> list(){
        return favoritiesService.getAll();
    }

    @GetMapping("/favorities/{id}")
    public Favorities edit(@PathVariable("id") Long id){
        return favoritiesService.getOne(id);
    }

    @PostMapping("/favorities")
    public Favorities save(@RequestBody Favorities favorities){
        return favoritiesService.saveOrUpdate(favorities);
    }

    @PutMapping("/favorities/update")
    public void update(@RequestBody Favorities favorities){
        favoritiesService.saveOrUpdate(favorities);
    }

    @DeleteMapping("/favorities/{id}")
    public void delete(@PathVariable("id") Long id){
        favoritiesService.remove(id);
    }
    
    @GetMapping("/favorities/getFavoritesVacants/{username}")
    public List <Vacant> getFavoritesVacants(@PathVariable("username") String username) {
    	return favoritiesService.getFavoritesVacants(username);
    }

}
