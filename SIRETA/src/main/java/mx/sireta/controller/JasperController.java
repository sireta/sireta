package mx.sireta.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.sireta.entity.Curriculum;
import mx.sireta.entity.WorkExperiences;
import mx.sireta.service.AbilitiesService;
import mx.sireta.service.ActivitiesService;
import mx.sireta.service.CourseService;
import mx.sireta.service.CertificationsService;
import mx.sireta.service.CurriculumService;
import mx.sireta.service.EducationService;
import mx.sireta.service.WorkExperiencesService;
import mx.sireta.service.LanguagesService;
import net.sf.jasperreports.engine.JRException;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperExportManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.data.JRBeanCollectionDataSource;
import net.sf.jasperreports.engine.util.JRLoader;
import net.sf.jasperreports.engine.JREmptyDataSource;

import java.io.File;
import java.io.FileInputStream;
import java.text.SimpleDateFormat;
import java.util.Collection;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.ByteArrayResource;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.util.ResourceUtils;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class JasperController {
    @Autowired
    private CurriculumService curriculumService;
    @Autowired
    private WorkExperiencesService workExperiencesService;
    @Autowired
    private EducationService educationService;
    @Autowired
    private CourseService courseService;
    @Autowired
    private AbilitiesService abilitiesService;
    @Autowired
    private CertificationsService certificationsService;
    @Autowired
    private LanguagesService LanguagesService;
    @Autowired
    private ActivitiesService activitiesService;

    @GetMapping("/generateCV/{id}")
    public ResponseEntity<byte[]> generatePdf(@PathVariable("id") Long id) throws Exception, JRException {
        try {
            Optional<Curriculum> curriculum = curriculumService.getCurriculumByApplicantsPersons_Id(id);
            if(!curriculum.isPresent()){
              return ResponseEntity.noContent().build();
            }

            //Consultas locas
            final File imgLogo = ResourceUtils.getFile("classpath:images/logo.png");
            Map<String, Object> parameters = new HashMap<>();

            parameters.put("nombre", curriculum.get().getApplicants().getPersons().getName());
            parameters.put("apellidos", curriculum.get().getApplicants().getPersons().getFirstLastName()+' '+curriculum.get().getApplicants().getPersons().getSecondLastName());
            parameters.put("puesto", curriculum.get().getTitle());
            parameters.put("telefono", curriculum.get().getApplicants().getPersons().getPhoneNumber());
            parameters.put("correo", curriculum.get().getApplicants().getPersons().getUsername());
            parameters.put("estado", curriculum.get().getApplicants().getState());
            parameters.put("imgLogo", new FileInputStream(imgLogo));
            //Consultas LISTAS DINAMICAS
            parameters.put("EducacionInvoice", new JRBeanCollectionDataSource((Collection<?>)  educationService.getEducationsByCurriculum(curriculum.get().getIdCurriculumGeneral())));
            parameters.put("ExperienciasInvoice", new JRBeanCollectionDataSource((Collection<?>)  workExperiencesService.getWorkExperienceByCurriculum(curriculum.get().getIdCurriculumGeneral())));
         //   parameters.put("ActividadesInvoice", new JRBeanCollectionDataSource((Collection<?>)  abilitiesService.getAbilitiesByPersonId(curriculum.get().getIdCurriculumGeneral())));
            parameters.put("CursosInvoice", new JRBeanCollectionDataSource((Collection<?>)  courseService.getCoursesByCurriculum(curriculum.get().getIdCurriculumGeneral())));
            parameters.put("CertificacionInvoice", new JRBeanCollectionDataSource((Collection<?>)  certificationsService.getAllByCurriculum(curriculum.get().getIdCurriculumGeneral())));

            parameters.put("IdiomasInvoice", new JRBeanCollectionDataSource((Collection<?>)  LanguagesService.getAllByCurriculum(curriculum.get().getIdCurriculumGeneral())));
            parameters.put("HabilidadesInvoice", new JRBeanCollectionDataSource((Collection<?>)  abilitiesService.getAbilitiesByPersonId(curriculum.get().getIdCurriculumGeneral())));

            JasperReport compileReport = JasperCompileManager
            .compileReport(new FileInputStream("src/main/resources/CV.jrxml"));
     
            JasperPrint report = JasperFillManager.fillReport(compileReport, parameters, new JREmptyDataSource());
            
            byte[] data = JasperExportManager.exportReportToPdf(report);
            HttpHeaders headers = new HttpHeaders();
            headers.add("Content-Disposition", "inline; filename=report.pdf");
            return ResponseEntity.ok().headers(headers).contentType(MediaType.APPLICATION_PDF).body(data);
          } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
