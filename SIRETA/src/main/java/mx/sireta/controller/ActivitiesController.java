package mx.sireta.controller;

import mx.sireta.entity.Activities;
import mx.sireta.security.ErrorHandler;
import mx.sireta.service.ActivitiesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/sireta")
public class ActivitiesController {

    @Autowired
    private ActivitiesService activitiesService;

    @GetMapping("/activities")
    public List<Activities> getActivities(){
        return activitiesService.getAll();
    }

    @PostMapping("/activities")
    public ResponseEntity<Object> save(@Valid  @RequestBody Activities activities, BindingResult result) {
        if (result.hasErrors()) {
            return new Validity().entity_IsValid(result);
        }
        try{
            activitiesService.saveOrUpdate(activities);
        }catch (Exception e){
            return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
        }
        return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, "Ok!");
    }

    @PutMapping("/activities")
    public Activities update(@RequestBody Activities activities) {
        return activitiesService.saveOrUpdate(activities);
    }

    @DeleteMapping("/activities/{id}")
    public void delete(@PathVariable("id") Long id) {
        activitiesService.remove(id);
    }

    @GetMapping("/activities/{id}")
    public void getActivities(@PathVariable("id") Long id) {
        activitiesService.getActivities(id);
    }

    @GetMapping("/activities/getAbilitiesByCurriculum/{id}")
    public List<Activities> getAbilitiesByCurriculum(@PathVariable("id") Long id){
        return activitiesService.getAbilitiesByCurriculum(id);
    }

}
