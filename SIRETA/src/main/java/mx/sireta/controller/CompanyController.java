package mx.sireta.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.support.SimpleTriggerContext;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Company;
import mx.sireta.service.CompanyService;
import org.springframework.web.multipart.MultipartFile;


@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class CompanyController {

    @Autowired
	private CompanyService companyService;

    @GetMapping("/company")
	public List<Company> getCompanies(){
		return companyService.getAll();
	}

	@GetMapping("/company/findByName/{name}")
	public boolean findByName(@PathVariable("name") String name){
    	return companyService.findByName(name);
	}

	@GetMapping("/company/findByNameExcludeThis/{id}/{name}")
	public boolean findByNameExcludeThis(@PathVariable("id") long id,@PathVariable("name") String name){
    	return companyService.findByNameExcludeThisId(name,id);
	}

	@DeleteMapping("/company/deleteByName/{name}")
	public void deleteByName(@PathVariable("name") String name){
		System.out.println("La compañia a eliminar es: " + name);
    	companyService.deleteByName(name);
	}

    @GetMapping("/company/{id}")
	public Company getCompany(@PathVariable("id") Long id){
		return companyService.getCompany(id);
	}

	@PostMapping("/company/save2")
	public Company save(@RequestParam("name") String name,
						@RequestParam("state") String state) {
		return companyService.saveOrUpdate(name, state);
	}

	@PostMapping("/company/save")
	public Company save(@RequestParam("name") String name,
						@RequestParam("state") String state,
						@RequestParam("logo") MultipartFile logo) throws IOException {
		return companyService.saveOrUpdate(name, state, logo);
	}

	//Aqui hay que agregarle el ID
	@PutMapping("/company/update/{idCompany}")
	public Company update(@PathVariable("idCompany") Integer idCompany,
						  @RequestParam("name") String name,
						  @RequestParam("state") String state,
						  @RequestParam("logo") MultipartFile logo) throws IOException {
		return companyService.saveOrUpdate(idCompany,name, state, logo);
	}

	@PutMapping("/company/update2/{idCompany}")
	public Company update(@PathVariable("idCompany") Integer idCompany,
						  @RequestParam("name") String name,
						  @RequestParam("state") String state) {
		return companyService.saveOrUpdate(idCompany,name, state);
	}
	
	@DeleteMapping("/company/{id}")
	public void delete(@PathVariable("id") Long id) {
		companyService.remove(id);
	}
}
