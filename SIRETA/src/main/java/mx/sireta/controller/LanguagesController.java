package mx.sireta.controller;

import java.util.List;

import mx.sireta.entity.Curriculum;
import mx.sireta.security.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.sireta.entity.Languages;
import mx.sireta.service.LanguagesService;

import javax.validation.Valid;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins= "*")
public class LanguagesController {

    @Autowired
    private LanguagesService LanguagesService;

    @GetMapping("/lenguajes")
    public List<Languages>list(){
        return LanguagesService.getAll();
    }

    @GetMapping("/lenguajes/{id}")
    public Languages edit(@PathVariable("id") Long id){
        return LanguagesService.getOne(id);
    }

    @GetMapping("/lenguajes/getAllByCurriculum/{id}")
    public List<Languages> getAllByCurriculum(@PathVariable("id") Long id){
        return LanguagesService.getAllByCurriculum(id);
    }

    @PostMapping("/lenguaje/save")
    public ResponseEntity<Object> save(@Valid @RequestBody Languages language, BindingResult result){
        if (result.hasErrors()) {
            String errors = "";
            for (ObjectError error : result.getAllErrors()) {
                errors = errors + error.getDefaultMessage() + "--";
            }
            return ErrorHandler.generateResponse("error", HttpStatus.OK, errors);
        }
        try{
            LanguagesService.saveOrUpdate(language);
        }catch (Exception e){
            return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
        }
        return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, "Ok!");
    }

    @PutMapping("/lenguaje/update")
    public void update(@RequestBody Languages language){
        LanguagesService.saveOrUpdate(language);
    }

    @DeleteMapping("/lenguaje/{id}")
    public void delete(@PathVariable("id") Long id){
        LanguagesService.remove(id);
    }
}
