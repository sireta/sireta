package mx.sireta.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import mx.sireta.entity.Contacs;
import mx.sireta.service.ContacsService;


@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class ContactsController {
    @Autowired
	private ContacsService contacsService;

	@GetMapping("/contact")
	public List<Contacs> getContacs(){
		return contacsService.getAll();
	}
    
    @GetMapping("/contact/{id}")
	public Contacs getContacs(@PathVariable("id") Long id){
		return contacsService.getContacs(id);
	}
    
	@PostMapping("/contact/save")
	public Contacs save(@RequestBody Contacs contacs) {
		return contacsService.saveOrUpdate(contacs);
	}

	@PutMapping("/contact/update")
	public Contacs update(@RequestBody Contacs contacs) {
		return contacsService.saveOrUpdate(contacs);
	}
	
	@DeleteMapping("/contact/{id}")
	public void delete(@PathVariable("id") Long id) {
		contacsService.remove(id);
	}
	
	@GetMapping("/contact/getContacts/{username}")
	public List<Contacs> getContacts(@PathVariable("username") String username) {
		return contacsService.getContacts(username);
	}
}
