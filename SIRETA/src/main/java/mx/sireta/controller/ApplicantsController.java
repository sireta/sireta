package mx.sireta.controller;

import mx.sireta.entity.Applicants;
import mx.sireta.entity.Persons;
import mx.sireta.entity.VacantByApplicant;
import mx.sireta.service.ApplicantsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/sireta")
public class ApplicantsController {

    @Autowired
    private ApplicantsService applicantsService;

    @GetMapping("/applicants")
    public List<Applicants> getApplicants(){
        return applicantsService.getAll();
    }

    @GetMapping("/applicant/user/{id}")
	public Applicants getByUserId(@PathVariable("id") Long id){
		return applicantsService.getApplicantByUserId(id);
	}

    @PostMapping("/applicants")
    public Applicants save(@RequestBody Applicants applicants) {
        return applicantsService.saveOrUpdate(applicants);
    }

    @PutMapping("/applicants")
    public Applicants update(@RequestBody Applicants applicants) {
        return applicantsService.saveOrUpdate(applicants);
    }

    @DeleteMapping("/applicants/{id}")
    public void delete(@PathVariable("id") Long id) {
        applicantsService.remove(id);
    }

    @GetMapping("/applicants/{id}")
    public void getWorkExperiences(@PathVariable("id") Long id) {
        applicantsService.getApplicants(id);
    }

    @GetMapping("/applicants/user/{id}")
    public Applicants getApplicantByUser(@PathVariable("id") Long id) {
        return applicantsService.getApplicantByUserId(id);
    }
    
    @GetMapping("/applicants/getApplicantsByRecruiter/{username}")
    public List<VacantByApplicant> getApplicantsByRecruiter(@PathVariable("username") String username) {
    	return applicantsService.getApplicantsByRecruiter(username);
    }

    @GetMapping("/applicant/idperson/{id}")
    public Applicants getApplicantByIdPerson(@PathVariable("id") Long id){
        return applicantsService.getApplicantsByPersons_Id(id);
    }

}
