package mx.sireta.controller;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Recruiters;
import mx.sireta.service.RecruitersService;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class RecruitersController {

	@Autowired
	private RecruitersService recruiterService;

	@GetMapping("/recruiters")
	public List<Recruiters> list(){
		return recruiterService.getAll();
	}

	@GetMapping("/recruiter/user/{id}")
	public Recruiters getByUserId(@PathVariable("id") Long id){
		return recruiterService.getByPersonId(id);
	}

	@GetMapping("/recruiters/{id}")
	public Recruiters edit(@PathVariable("id") Long id){
		return recruiterService.getOne(id);
	}

	@PostMapping("/recruiters")
	public Recruiters save(@RequestBody Recruiters recruiter){
		return recruiterService.saveOrUpdate(recruiter);
	}

	@PutMapping("/recruiters/update")
	public void update(@RequestBody Recruiters recruiter){
		recruiterService.saveOrUpdate(recruiter);
	}

	@DeleteMapping("/recruiters/{id}")
	public void delete(@PathVariable("id") Long id){
		recruiterService.remove(id);
	}
	
}
