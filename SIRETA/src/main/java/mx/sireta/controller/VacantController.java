package mx.sireta.controller;

import java.util.List;

import mx.sireta.entity.Benefits;
import mx.sireta.entity.Salaries;
import mx.sireta.security.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Vacant;
import mx.sireta.service.VacantService;

import javax.validation.Valid;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class VacantController {

    @Autowired
	private VacantService vacantService;

	@GetMapping("/vacants")
	public List<Vacant> getVacants(){
		return vacantService.getAll();
	}
    
    @GetMapping("/vacants/{id}")
	public Vacant getVacant(@PathVariable("id") Long id){
		return vacantService.getVacant(id);
	}
    
	@PostMapping("/vacants/save")
	public ResponseEntity<Object> save(@Valid @RequestBody Vacant vacant, BindingResult result) {
		if (result.hasErrors()) {
			String errors = "";
			for (ObjectError error : result.getAllErrors()) {
				errors = errors + error.getDefaultMessage() + "--";
			}
			return ErrorHandler.generateResponse("error", HttpStatus.OK, errors);
		}
		Vacant newVacant = new Vacant();
		try{
			newVacant = vacantService.saveOrUpdate(vacant);
		}catch (Exception e){
			return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, newVacant);
	}

	@PutMapping("/vacants/update")
	public Vacant update(@RequestBody Vacant vacant) {
		return vacantService.saveOrUpdate(vacant);
	}
	
	@DeleteMapping("/vacants/{id}")
	public void delete(@PathVariable("id") Long id) {
		vacantService.remove(id);
	}
	
	@GetMapping("/getMyVacants/{username}")
	public List<Vacant> getMyVacants(@PathVariable("username") String username){
		return vacantService.getVacantsByRecruiter(username);
	}
	
	@GetMapping("/getVacants/{username}")
	public List<Vacant> getVacantsByState(@PathVariable("username") String username){
		return vacantService.getVacantsByState(username);
	}
	
	@GetMapping("/filterVacants/{username}/{filter}")
	public List<Vacant> getVacantsByFilter(@PathVariable("username") String username, @PathVariable("filter") String filter){
		System.out.print("dtoas: " +  username + ", " + filter);
		return vacantService.getVacantsByFilter(username, filter);
	}
	
}
