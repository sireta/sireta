package mx.sireta.controller;

import java.util.List;

import mx.sireta.security.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Salaries;
import mx.sireta.service.SalariesService;

import javax.validation.Valid;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class SalariesController {

    @Autowired
	private SalariesService salariesService;

	@GetMapping("/salaries")
	public List<Salaries> gSalaries(){
		return salariesService.getAll();
	}
    
    @GetMapping("/salaries/{id}")
	public Salaries getCourse(@PathVariable("id") Long id){
		return salariesService.getSalaries(id);
	}
    
	@PostMapping("/salaries/save")
	public ResponseEntity<Object> save(@Valid @RequestBody Salaries salaries, BindingResult result) {
		if (result.hasErrors()) {
			String errors = "";
			for (ObjectError error : result.getAllErrors()) {
				errors = errors + error.getDefaultMessage() + "--";
			}
			return ErrorHandler.generateResponse("error", HttpStatus.OK, errors);
		}
		Salaries newSalary = new Salaries();
		try{
			newSalary = salariesService.saveOrUpdate(salaries);
		}catch (Exception e){
			return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, newSalary);
	}

	@PutMapping("/salaries/update")
	public Salaries update(@RequestBody Salaries salaries) {
		return salariesService.saveOrUpdate(salaries);
	}
	
	@DeleteMapping("/salaries/{id}")
	public void delete(@PathVariable("id") Long id) {
		salariesService.remove(id);
	}
}
