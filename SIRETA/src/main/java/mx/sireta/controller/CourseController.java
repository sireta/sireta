package mx.sireta.controller;

import java.util.List;

import mx.sireta.security.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Course;
import mx.sireta.service.CourseService;

import javax.validation.Valid;


@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class CourseController {

    @Autowired
	private CourseService courseService;

	@GetMapping("/courses")
	public List<Course> getCourses(){
		return courseService.getAll();
	}
    
    @GetMapping("/course/{id}")
	public Course getCourse(@PathVariable("id") Long id){
		return courseService.getCourse(id);
	}
    
	@PostMapping("/course/save")
	public ResponseEntity<Object> save(@Valid @RequestBody Course course, BindingResult result) {
		if(result.hasErrors()){
			return new Validity().entity_IsValid(result);
		}
		try{
			courseService.saveOrUpdate(course);
		}catch (Exception e){
			return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, "Ok!");
	}

	@PutMapping("/course/update")
	public Course update(@RequestBody Course course) {
		return courseService.saveOrUpdate(course);
	}

	@GetMapping("/course/getCoursesByCurriculum/{id}")
	public List<Course> getCoursesByCurriculum(@PathVariable("id") Long id){
		return courseService.getCoursesByCurriculum(id);
	}

	@DeleteMapping("/course/{id}")
	public void delete(@PathVariable("id") Long id) {
		courseService.remove(id);
	}
}
