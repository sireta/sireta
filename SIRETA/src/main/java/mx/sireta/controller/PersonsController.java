package mx.sireta.controller;

import java.util.List;

import mx.sireta.security.ErrorHandler;
import mx.sireta.service.PersonsService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;
import mx.sireta.entity.Persons;

import javax.validation.Valid;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class PersonsController {
	
	@Autowired
	private PersonsService personService;

	@Autowired
	private PasswordEncoder passwordEncoder;

	@GetMapping("/persons/rol/{username}")
	public String getRolByUsername(@PathVariable("username") String username){
		return personService.getRoleByUsername(username);
	}

	@GetMapping("/persons/user/{username}")
	public Persons getByUsername(@PathVariable("username") String username){
		return personService.getByUsername(username);
	}

	@GetMapping("/persons")
	public List<Persons> list(){
		return personService.getAll();
	}

	@GetMapping("/persons/{id}")
	public Persons edit(@PathVariable("id") Long id){
		return personService.getOne(id);
	}

	@PostMapping("/persons")
	public ResponseEntity<Object> save(@Valid @RequestBody Persons person, BindingResult result){
		if (result.hasErrors()) {
			String errors = "";
			for (ObjectError error : result.getAllErrors()) {
				errors = errors + error.getDefaultMessage() + "--";
			}
			return ErrorHandler.generateResponse("error", HttpStatus.OK, errors);
		}
		Persons newPerson = new Persons();
		if ( person.getId() == null) {
			person.setPassword(passwordEncoder.encode(person.getPassword()));
		}
		try{
			newPerson = personService.saveOrUpdate(person);
		}catch (Exception e){
			return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, newPerson);
	}

	@PutMapping("/persons/update/{idPerson}")
	public ResponseEntity<Object> update(@RequestParam("idPerson") long idPerson, @RequestBody Persons person){

		Persons newPerson = new Persons();
		person.setPassword(passwordEncoder.encode(person.getPassword()));
		try{
			newPerson = personService.saveOrUpdate(person);
		}catch (Exception e){
			return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, newPerson);
	}

	@DeleteMapping("/persons/{id}")
	public void delete(@PathVariable("id") Long id){
		personService.remove(id);
	}

	@PutMapping("/persons/updatePassword/{idPerson}")
	public ResponseEntity<Object> updatePassword(@RequestParam("idPerson") long idPerson,
												@RequestParam("password") String password,
												@RequestParam("repassword") String repassword ){
		if (!password.equals(repassword)) {
			return ErrorHandler.generateResponse("Las Contraseñas no coinciden", HttpStatus.MULTI_STATUS, "");
		}											
		Persons person = personService.getOne(idPerson);
		person.setPassword(passwordEncoder.encode(password));
		try{
			person = personService.saveOrUpdate(person);
		}catch (Exception e){
			return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, person);
	}
	
	@GetMapping("/persons/getAllPersons")
	public List<Persons> getAllPersons() {
		SecurityContext securityContext = SecurityContextHolder.getContext();
		Authentication authentication = securityContext.getAuthentication();
		String username = "";
		if (authentication != null) {
			username = authentication.getName();
		}
		return personService.getAllPersons(username);
	}
}
