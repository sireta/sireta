package mx.sireta.controller;

import java.util.List;

import mx.sireta.security.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import mx.sireta.entity.Certifications;
import mx.sireta.service.CertificationsService;

import javax.validation.Valid;

@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class CertificationsController {

    @Autowired
    private CertificationsService certificationsService;

    @GetMapping("/certificaciones")
    public List<Certifications> list(){
        return certificationsService.getAll();
    }

    @GetMapping("/certificaciones/{id}")
    public Certifications edit(@PathVariable("id") Long id){
        return certificationsService.getOne(id);
    }

    @GetMapping("/certificacion/getAllByCurriculum/{id}")
    public List<Certifications> getAllByCurriculum(@PathVariable("id") Long id){
        return certificationsService.getAllByCurriculum(id);
    }

    @PostMapping("/certificacion/save")
    public ResponseEntity<Object> save(@Valid @RequestBody Certifications certification, BindingResult result){
        if (result.hasErrors()) {
            String errors = "";
            for (ObjectError error : result.getAllErrors()) {
                errors = errors + error.getDefaultMessage() + "--";
            }
            return ErrorHandler.generateResponse("error", HttpStatus.OK, errors);
        }
        try{
            certificationsService.saveOrUpdate(certification);
        }catch (Exception e){
            return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
        }
        return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, "Ok!");
    }

    @PutMapping("/certificacion/update)")
    public void update(@RequestBody Certifications certification){
        certificationsService.saveOrUpdate(certification);
    }

    @DeleteMapping("/certificacion/{id}")
    public void delete(@PathVariable("id") Long id){
        certificationsService.remove(id);
    }
}
