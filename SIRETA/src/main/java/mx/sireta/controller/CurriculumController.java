package mx.sireta.controller;

import java.util.List;
import java.util.Optional;

import mx.sireta.entity.Persons;
import mx.sireta.security.ErrorHandler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.annotation.*;

import mx.sireta.entity.Curriculum;
import mx.sireta.service.CurriculumService;

import javax.validation.Valid;


@RestController
@RequestMapping("/sireta")
@CrossOrigin(origins = "*")
public class CurriculumController {

    @Autowired
	private CurriculumService curriculumService;

	@GetMapping("/curriculum")
	public List<Curriculum> getCurriculum(){
		return curriculumService.getAll();
	}
    
    @GetMapping("/curriculum/{id}")
	public Curriculum getCurriculum(@PathVariable("id") Long id){
		return curriculumService.getCurriculum(id);
	}
    
	@PostMapping("/curriculum/save")
	public ResponseEntity<Object> save(@Valid  @RequestBody Curriculum curriculum, BindingResult result) {
		if (result.hasErrors()) {
			String errors = "";
			for (ObjectError error : result.getAllErrors()) {
				errors = errors + error.getDefaultMessage() + "--";
			}
			return ErrorHandler.generateResponse("error", HttpStatus.OK, errors);
		}
		Curriculum newCurriculum;
		try{
			newCurriculum = curriculumService.saveOrUpdate(curriculum);
		}catch (Exception e){
			return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, newCurriculum);
	}

	@PutMapping("/curriculum/update")
	public Curriculum update(@RequestBody Curriculum curriculum) {
		return curriculumService.saveOrUpdate(curriculum);
	}
	
	@DeleteMapping("/curriculum/{id}")
	public void delete(@PathVariable("id") Long id) {
		curriculumService.remove(id);
	}

	@GetMapping("/curriculum/person/{id}")
	public ResponseEntity<Object> getCurriculumByPerson(@PathVariable("id") Long id){
		Optional<Curriculum> curriculum = curriculumService.getCurriculumByApplicantsPersons_Id(id);
		if(curriculum.isPresent()){
			return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, curriculum.get());
		}
		return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, new Curriculum());
	}

}
