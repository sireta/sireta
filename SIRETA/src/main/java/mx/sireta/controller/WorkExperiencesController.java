package mx.sireta.controller;

import mx.sireta.entity.WorkExperiences;
import mx.sireta.security.ErrorHandler;
import mx.sireta.service.WorkExperiencesService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@CrossOrigin(origins = "*")
@RequestMapping("/sireta")
public class WorkExperiencesController {

    @Autowired
    private WorkExperiencesService workExperiencesService;

    @GetMapping("/workExperiences")
    public List<WorkExperiences> getWorkExperiences(){
        return workExperiencesService.getAll();
    }

    @PostMapping("/workExperiences")
    public ResponseEntity<Object> save(@Valid @RequestBody WorkExperiences workExperiences, BindingResult result) {
        if(result.hasErrors()){
            return new Validity().entity_IsValid(result);
        }
        try{
            workExperiencesService.saveOrUpdate(workExperiences);
        }catch (Exception e){
            return ErrorHandler.generateResponse(e.getMessage(), HttpStatus.MULTI_STATUS, "");
        }
        return ErrorHandler.generateResponse("Ok!", HttpStatus.OK, "Ok!");
    }

    @PutMapping("/workExperiences")
    public WorkExperiences update(@RequestBody WorkExperiences workExperiences) {
        return workExperiencesService.saveOrUpdate(workExperiences);
    }

    @GetMapping("/workExperiences/getWorkExperienceByCurriculum/{id}")
    public List<WorkExperiences> getWorkExperienceByCurriculum(@PathVariable("id") Long id){
        return workExperiencesService.getWorkExperienceByCurriculum(id);
    }

    @DeleteMapping("/workExperiences/{id}")
    public void delete(@PathVariable("id") Long id) {
        workExperiencesService.remove(id);
    }

    @GetMapping("/workExperiences/{id}")
    public void getWorkExperiences(@PathVariable("id") Long id) {
        workExperiencesService.getWorkExperiences(id);
    }

}
