package mx.sireta.security;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import mx.sireta.entity.Persons;
import mx.sireta.repository.PersonsRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Component;

import java.io.Serializable;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Component
public class JwtTokenUtil implements Serializable {

    @Autowired
    private PersonsRepository personsRepository;

    private static final long serialVersionUID = -2550185165626007488L;

    //Duración del token
    public static final long JWT_TOKEN_VALIDITY = 5 * 60 * 60;

    //Secret key -> Está declarada en el Application.properties
    @Value("${jwt.secret}")
    private String secret;

    //Obtiene el usuario por el token
    public String getUsernameFromToken(String token) {
        return getClaimFromToken(token, Claims::getSubject);
    }

    //La fecha de expiración del token
    public Date getExpirationDateFromToken(String token) {
        return getClaimFromToken(token, Claims::getExpiration);
    }

    public <T> T getClaimFromToken(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = getAllClaimsFromToken(token);
        return claimsResolver.apply(claims);
    }
    //Secret Key
    private Claims getAllClaimsFromToken(String token) {
        return Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
    }

    //Verifica si el token no ha expirado
    private Boolean isTokenExpired(String token) {
        final Date expiration = getExpirationDateFromToken(token);
        return expiration.before(new Date());
    }

    //Generación del token
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        Persons usuario = personsRepository.findPersonsByUsername(userDetails.getUsername());
        return doGenerateToken(claims, userDetails.getUsername(), usuario.getId());
    }

    private String doGenerateToken(Map<String, Object> claims, String subject, Long id) {

        return Jwts.builder().setClaims(claims).setSubject(subject).setId(Long.toString(id)).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + JWT_TOKEN_VALIDITY * 1000))
                .signWith(SignatureAlgorithm.HS512, secret).compact();
    }

    //validación del token
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = getUsernameFromToken(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }
}
