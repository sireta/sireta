package mx.sireta.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import lombok.Data;

@Data
@Entity
@Table(name = "recruiters")
public class Recruiters {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idRecruiter")
	private Long id;
	
	@Column(nullable = false, length = 45)
	private String job;
	
	@OneToOne
	@JoinColumn(name = "person_idPerson")
	private Persons person;
	
	@OneToOne
	@JoinColumn(name = "company_idCompany")
	private Company company;
	
}
