package mx.sireta.entity;

import javax.persistence.*;
import java.util.Date;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Past;
import javax.validation.constraints.PastOrPresent;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "education")
public class Education {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idEducation")
	private Long idEducation;

    @Column(nullable = false, name = "educationalDegree", length = 45)
	@NotBlank(message = "El título educativo es obligatorio")
    @Size(max = 45, message = "El título educativo debe tener máximo 45 caracteres")
	private String educationalDegree;

    @Column(nullable = false, length = 50)
	@NotBlank(message = "El nombre de la carrera es obligatorio")
    @Size(max = 50, message = "El nombre de la carrera debe tener máximo 50 caracteres")
	private String career;

	@Column(nullable = false, name = "instituteName", length = 45)
	@NotBlank(message = "El nombre de la escuela es obligatorio")
    @Size(max = 45, message = "El nombre de la escuela debe tener máximo 45 caracteres")
	private String instituteName;

	@Temporal(TemporalType.DATE)
	@Column(name = "initialDate")
	@Past(message = "La fecha inicial debe ser pasada")
	private Date initialDate;

    @Temporal(TemporalType.DATE)
	@Column(name = "finalDate")
	@PastOrPresent(message = "La fecha final debe ser pasada")
	private Date finalDate;

	@ManyToOne
	@JoinColumn(name = "curriculum_idCurriculum")
	private Curriculum curriculum;

}
