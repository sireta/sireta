package mx.sireta.entity;

import lombok.Data;
import java.io.Serializable;
import javax.persistence.*;

@Data
@Entity
@Table(name = "benefits")
public class Benefits implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idBenefit")
    private Long id;

    @Column(nullable = false, length = 50)
    private String benefit;

    @ManyToOne
    @JoinColumn(name = "vacant_idVacant")
    private Vacant vacant;

}
