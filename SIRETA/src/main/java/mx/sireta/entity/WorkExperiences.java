package mx.sireta.entity;

import lombok.Data;
import javax.persistence.*;
import java.util.Date;
import javax.validation.constraints.*;

@Data
@Entity
@Table(name = "workExperiences")
public class WorkExperiences {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idWorkExperience")
    private Long idWorkExperience;

    @Column(nullable = false, length = 45)
    @NotBlank(message = "El nombre del trabajo es obligatorio")
    @Size(max = 45, message = "El nombre del trabajo debe tener máximo 45 caracteres")
    private String job;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, name = "initialDate")
    @NotNull(message = "La fecha de inicio es obligatoria")
    @Past(message = "La fecha de inicio debe ser pasada")
    private Date initialDate;

    @Temporal(TemporalType.DATE)
    @Column(nullable = false, name = "finalDate")
    @NotNull(message = "La fecha final es obligatoria")
    @PastOrPresent(message = "La fecha final debe ser pasada")
    private Date finalDate;

    @ManyToOne
    @JoinColumn(name = "curriculum_idCurriculum")
    private Curriculum curriculum;

}
