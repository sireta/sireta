package mx.sireta.entity;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "applicants")
public class Applicants {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idApplicant;

    @Column(nullable = false, length = 20)
    private String state;

    @OneToOne
    @JoinColumn(name = "persons_idPersons")
    private Persons persons;

}
