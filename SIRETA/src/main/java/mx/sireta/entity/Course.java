package mx.sireta.entity;
import javax.persistence.*;
import java.util.Date;
import lombok.Data;

import javax.validation.constraints.*;

@Data
@Entity
@Table(name = "courses")
public class Course {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCourse")
	private Long idCourse;

    @Column(nullable = false, length = 45)
	@NotBlank(message = "El nombre del curso es obligatorio")
    @Size(max = 45, message = "El nombre del curso debe tener máximo 45 caracteres")
	private String name;
    
    @Column(nullable = false, name = "hoursNumber", length = 4)
	@NotNull(message = "El número de horas es obligatorio")
	@Min(value = 10, message = "El número de horas debe ser mayor a 10")
	@Max(value = 9999, message = "El número de horas debe ser menor a 1000")
	private Integer hoursNumber;

    @Column(nullable = false, name = "issuingInstitute", length = 50)
	@NotBlank(message = "El nombre de la institución emisora es obligatorio")
    @Size(max = 50, message = "El nombre de la institución emisora debe tener máximo 50 caracteres")
	private String issuingInstitute;

	@Temporal(TemporalType.DATE)
    @Column(name = "realizationDate")
	@PastOrPresent(message = "La fecha de obtención debe ser una fecha pasada")
	private Date realizationDate;

	@ManyToOne
	@JoinColumn(name = "curriculum_idCurriculum")
	private Curriculum curriculum;

}
