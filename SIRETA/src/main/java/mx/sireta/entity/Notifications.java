package mx.sireta.entity;

import javax.persistence.*;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;

import javax.validation.constraints.Future;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.util.Date;

@Data
@Entity
@Table(name = "notifications")
public class Notifications {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idNotification")
	private Long id;
	
	@Column(nullable = false, length = 45)
	@NotBlank(message = "El titulo de la notificación es obligatorio")
    @Size(max = 45, message = "El titulo de la notificación debe tener máximo 45 caracteres")
	private String title; 
	
	@Column(nullable = false, length = 45)
	@NotBlank(message = "El nombre de la institución es obligatorio")
    @Size(max = 45, message = "El nombre de la institución debe tener máximo 45 caracteres")
	private String message;
	
	@Column(nullable = false, name = "isOpen", columnDefinition = "tinyint not null")
	private boolean isOpen;

	@CreationTimestamp
	@Temporal(TemporalType.TIMESTAMP)
	@Column(name = "date", nullable = false)
	@Future
	private Date date;
	
	@ManyToOne
	@JoinColumn(name = "person_Sender")
	private Persons person_Sender;
	
	@ManyToOne
	@JoinColumn(name = "person_Receiver")
	private Persons person_Receiver;

}
