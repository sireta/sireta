package mx.sireta.entity;

import lombok.Data;
import java.util.Date;
import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "persons")
@Data
public class Persons {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idPerson")
    private Long id;

	@Pattern(regexp = "[a-zA-ZñÑáéíóúÁÉÍÓÚ\\s]+", message = "El nombre no puede contener números y/o caracteres especiales")
	@NotBlank(message = "El nombre es obligatorio")
	@Size(max = 45, message = "El nombre debe tener máximo 45 caracteres")
	@Column(nullable = false, length = 45)
	private String name;

	@Pattern(regexp = "[a-zA-ZñÑáéíóúÁÉÍÓÚ\\s]+", message = "El primer apellido no puede contener números y/o caracteres especiales")
	@NotBlank(message = "El primer apellido es obligatorio")
	@Size(max = 45, message = "El primer apellido debe tener máximo 45 caracteres")
	@Column(name = "firstLastName", nullable = false, length = 45)
	private String firstLastName;

	@Size(max = 45, message = "El segundo apellido debe tener máximo 45 caracteres")
	@Column(name = "secondLastName", length = 45)
	private String SecondLastName;

	@Email(message = "El correo electrónico no es válido")
	@NotBlank(message = "El correo electrónico es obligatorio")
	@Size(max = 100, message = "El correo electrónico debe tener máximo 100 caracteres")
    @Column(nullable = false, length = 100, unique = true)
    private String username;

	@Pattern(regexp = "[0-9]+", message = "El número de teléfono no puede contener letras")
	@NotBlank(message = "El número de teléfono es obligatorio")
	@Size(max = 45, message = "El número de teléfono debe tener máximo 10 caracteres")
	@Column(name = "phoneNumber", nullable = false, length = 10, unique = true)
	private String phoneNumber;

	@Temporal(TemporalType.DATE)
	@Column(name = "birthDate", nullable = false)
	private Date birthDate;

	@Column(columnDefinition = "tinyint not null")
	private boolean enabled;

	@Pattern(regexp = "^(?=.*\\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[a-zA-Z]).{8,}$", message = "La contraseña debe contener al menos una mayúscula, una minúscula, un número y un caracter especial")
	@NotBlank(message = "La contraseña es obligatoria")
    @Column(length = 100, nullable = false)
    private String password;

	@ManyToOne
	@JoinColumn(name = "rol_idRol")
	private Rol rol;

}
