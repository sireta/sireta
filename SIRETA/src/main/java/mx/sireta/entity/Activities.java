package mx.sireta.entity;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "activities")
public class Activities {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long idActivity;

    @Column(nullable = false, length = 60)
    @NotBlank(message = "El nombre de la actividad es obligatorio")
    @Size(max = 60, message = "La actividad debe tener máximo 60 caracteres")
    private String activity;

    @ManyToOne
    @JoinColumn(name = "workExperience_idWorkExperience")
    private WorkExperiences workExperiences;

}
