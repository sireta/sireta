package mx.sireta.entity;

import javax.persistence.*;
import java.util.Date;
import lombok.Data;
import org.hibernate.annotations.CreationTimestamp;
import javax.validation.constraints.FutureOrPresent;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;

@Data
@Entity
@Table(name = "vacants")
public class Vacant {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idVacant")
	private Long idVacant;

    @Column(nullable = false, length = 45)
	@NotBlank(message = "El título es obligatorio")
	private String title;

    @Column(nullable = false)
	@NotBlank(message = "La descripción es obligatorio")
	private String description;

	@Column(nullable = false, length = 10)
	@NotBlank(message = "La modalidad es obligatorio")
	private String modality;

    @Column(nullable = false, length = 15)
	@NotBlank(message = "El tipo es obligatorio")
	private String type;

	@Temporal(TemporalType.DATE)
	@Column(nullable = false, name = "initialDate")
	@NotNull(message = "La fecha de inicio es obligatoria")
	@FutureOrPresent(message = "La fecha de inicio debe ser actual o futura")
	private Date initialDate;

    @Temporal(TemporalType.DATE)
	@Column(nullable = false, name = "validityPeriod")
	@NotNull(message = "La fecha final es obligatoria")
	@FutureOrPresent(message = "La fecha final debe ser actual o futura")
	private Date validityPeriod;

    @Temporal(TemporalType.TIMESTAMP)
	@Column(name = "creationDate")
	@CreationTimestamp
	private Date creationDate;

	@Column(nullable = false, length = 20)
	@NotBlank(message = "El estado es obligatorio")
	private String state;

	@OneToOne
	@JoinColumn(name = "salary_idSalary")
	private Salaries salary;

	@ManyToOne
	@JoinColumn(name = "recruiter_idRecruiter")
	private Recruiters recruiter;

}
