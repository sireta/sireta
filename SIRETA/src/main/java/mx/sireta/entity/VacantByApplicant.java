package mx.sireta.entity;

import lombok.Data;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "vacantByApplicant")
public class VacantByApplicant {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idVacantByApplicant")
    private Long idVacantByApplicant;

    @Column(name = "isOpenCV", columnDefinition = "tinyint not null")
    private boolean isOpenCV;

    @Lob
    @Column(name = "curriculumPDF")
    private byte[] curriculumPDF;

    @Column(length = 15, nullable = false)
    @NotBlank(message = "El estatus obligatorio")
    @Size(max = 15, message = "El estatus debe tener máximo 15 caracteres")
    private String status;

    @ManyToOne
    @JoinColumn(name = "applicant_idApplicant")
    private Applicants applicants;

    @ManyToOne
    @JoinColumn(name = "vacant_idVacant")
    private Vacant vacant;

}
