package mx.sireta.entity;
import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotBlank;
import lombok.Data;

@Data
@Entity
@Table(name = "salaries")
public class Salaries {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idSalary")
	private Long idSalary;

    @Column(nullable = false)
    @Min(value = 1, message = "El salario mínimo debe ser mayor a 1")
    @Max(value = 100000, message = "El salario mínimo debe ser menor a 100000")
    private Float minimum;
    
    @Column(nullable = false)
    @Min(value = 1, message = "El salario máximo debe ser mayor a 1")
    @Max(value = 100000, message = "El salario máximo debe ser menor a 100000")
	private Float maximum;

    @Column(nullable = false, length = 10)
    @NotBlank(message = "La frecuencia es obligatoria")
	private String periodicity;

}
