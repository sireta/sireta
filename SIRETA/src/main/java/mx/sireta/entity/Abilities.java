package mx.sireta.entity;

import java.io.Serializable;
import javax.persistence.*;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

import lombok.Data;

@Entity
@Table(name = "abilities")
@Data
public class Abilities implements Serializable{
    
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idAbility")
    private Long id;

    @Column(nullable = false, length = 45)
    @NotBlank(message = "El nombre de la habilidad es obligatorio")
    @Size(max = 45, message = "El nombre de la habilidad debe tener máximo 45 caracteres")
    private String name;

    @ManyToOne
    @JoinColumn(name = "curriculum_idcurriculum")
    private Curriculum curriculum;
}
