package mx.sireta.entity;

import java.io.Serializable;
import javax.persistence.*;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;

@Entity
@Table(name = "languages")
@Data
public class Languages  implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idLanguage")
    private Long id;

    @Column(nullable = false, length = 20)
    @NotBlank(message = "El nombre del idioma es obligatorio")
    @Size(max = 20, message = "El nombre del idioma debe tener máximo 20 caracteres")
    private String name;

    @Column(nullable = false, length = 10)
    @NotBlank(message = "El nivel del idioma es obligatorio")
    @Size(max = 10, message = "El nivel del idioma debe tener máximo 10 caracteres")
    private String level;

    @ManyToOne
    @JoinColumn(name = "curriculum_idCurriculum")
    private Curriculum curriculum;

}
