package mx.sireta.entity;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "favorities")
public class Favorities {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idFavority")
    private Long idFavority;

    @ManyToOne
    @JoinColumn(name = "applicant_idApplicant")
    private Applicants applicants;

    @ManyToOne
    @JoinColumn(name = "vacant_idVacant")
    private Vacant vacant;


}
