package mx.sireta.entity;
import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "contacs")
public class Contacs {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idContac")
	private Long idContac;

    @ManyToOne
    @JoinColumn(name = "person_Main")
    private Persons person_Main;

    @ManyToOne
    @JoinColumn(name = "person_Friend")
    private Persons person_Friend;
	
}
