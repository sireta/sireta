package mx.sireta.entity;

import java.io.Serializable;
import java.util.Date;
import javax.validation.constraints.*;
import javax.persistence.*;
import lombok.Data;

@Entity
@Table(name="certifications")
@Data
public class Certifications implements Serializable{

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCertification")
    private Long id;

    @Column(nullable = false, length = 50)
    @NotBlank(message = "El nombre del certificado es obligatorio")
    @Size(max = 50, message = "El nombre del certificado debe tener máximo 50 caracteres")
    private String name;

    @Temporal(TemporalType.DATE)
    @Column(name = "obtainedDate", nullable = false)
    @NotNull(message = "La fecha de obtención es obligatoria")
    @PastOrPresent(message = "La fecha de obtención debe ser anterior a la actual")
    private Date obtainedDate;

    @Temporal(TemporalType.DATE)
    @Column(name = "expirationDate")
    @FutureOrPresent(message = "La fecha de expiración debe ser futura")
    private Date expirationDate;

    @ManyToOne
    @JoinColumn(name = "curriculum_idCurriculum")
    private Curriculum curriculum;

}
