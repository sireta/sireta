package mx.sireta.entity;

import lombok.Data;
import javax.persistence.*;

@Data
@Entity
@Table(name = "rol")
public class Rol {

    @Id
    @GeneratedValue(strategy= GenerationType.IDENTITY)
    private int id_rol;

    @Column(name = "nombre", length = 21)
    private String nombre;

}
