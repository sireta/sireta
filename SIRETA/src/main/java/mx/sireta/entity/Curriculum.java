package mx.sireta.entity;

import javax.persistence.*;
import lombok.Data;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;

@Data
@Entity
@Table(name = "curriculum")
public class Curriculum {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCurriculum")
	private Long idCurriculumGeneral;

    @Column(nullable = false, length = 45)
    @NotBlank(message = "El nombre del título es obligatorio")
    @Size(max = 45, message = "El nombre del título debe tener máximo 45 caracteres")
	private String title;

    @OneToOne
    @JoinColumn(name = "applicant_idApplicant")
    private Applicants applicants;

}
