package mx.sireta.entity;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "companies")
public class Company {

    @Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "idCompany")
	private Long idCompany;

    @Column(nullable = false, length = 60)
	private String name;

    @Column(nullable = false, length = 20)
	private String state;

    @Lob
    @Column()
	private byte[] logo;

}
