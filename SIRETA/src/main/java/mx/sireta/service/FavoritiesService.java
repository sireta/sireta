package mx.sireta.service;

import mx.sireta.entity.Favorities;
import mx.sireta.entity.Vacant;
import mx.sireta.repository.FavoritiesRepository;
import mx.sireta.repository.VacantRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class FavoritiesService {

    @Autowired
    private FavoritiesRepository favoritiesRepository;
    
    @Autowired
    private VacantRepository vacantsRepository;

    public List<Favorities> getAll(){
        return favoritiesRepository.findAll();
    }

    public Favorities getOne(Long id){
        return favoritiesRepository.findById(id).get();
    }

    public Favorities saveOrUpdate(Favorities favorities){
        return favoritiesRepository.save(favorities);
    }

    public void remove(Long id){
        favoritiesRepository.deleteById(id);
    }
    
    public List <Vacant> getFavoritesVacants(String username){
    	return vacantsRepository.getFavoritesVacants(username);
    }

}
