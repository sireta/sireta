package mx.sireta.service;

import mx.sireta.entity.Persons;
import mx.sireta.repository.PersonsRepository;
import mx.sireta.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

@Service
public class PersonsService implements UserDetailsService {

    //NO MODIFICAR ESTOS MÉTODOS
    //Solo añadir los métodos necesarios para un CRUD

    @Autowired
    private RolRepository rolRepository;

    @Autowired
    private PasswordEncoder passwordEncoder;

    @Autowired
    private PersonsRepository personsRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        Persons usuario = personsRepository.findPersonsByUsername(username);
        if (usuario == null){
            throw new UsernameNotFoundException("Usuario no encontrado con el siguiente username: " + username);
        }
        return new org.springframework.security.core.userdetails.User(usuario.getUsername(), usuario.getPassword(), getAuthority());
    }

    //Si tuvieramos más roles aquí los especificariamos por medio de una consulta
    //De momento probar con el rol de recruiter -> falta el de applicant, El general se probará con las rutas abiertas
    private Set<SimpleGrantedAuthority> getAuthority(){
        Set<SimpleGrantedAuthority> authorities = new HashSet<>();
        authorities.add(new SimpleGrantedAuthority("ROLE_" + rolRepository.getRoles("recruiter").getNombre()));
        return authorities;
    }

    public List<Persons> getAll(){
        return personsRepository.findAll();
    }

    public Persons getOne(Long id){
        return personsRepository.findById(id).get();
    }

    public Persons saveOrUpdate(Persons persons){
        return personsRepository.save(persons);
    }

    public void remove(Long id){
        personsRepository.deleteById(id);
    }

    public String getRoleByUsername(String username){
        return personsRepository.findPersonsByUsername(username).getRol().getNombre();
    }

    public void updatePasswordByUsername(String username, String newPassword){
        newPassword = passwordEncoder.encode(newPassword);
        personsRepository.updatePassword(newPassword, username);
    }

    public Persons getByUsername(String username){
        return personsRepository.findPersonsByUsername(username);
    }

    public List<Persons> getAllPersons(String username) {
		return personsRepository.getAllPersons(username);
    }
}
