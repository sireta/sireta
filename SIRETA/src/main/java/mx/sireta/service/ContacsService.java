package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Contacs;
import mx.sireta.entity.Persons;
import mx.sireta.repository.ContacsRepository;
import mx.sireta.repository.PersonsRepository;

@Service
public class ContacsService {

    @Autowired
	private ContacsRepository contacsRepository;

    public List<Contacs> getAll(){
		return contacsRepository.findAll();
	}

	public Contacs getContacs(Long id) {
	 	return contacsRepository.findById(id).get();
	}
		
	public Contacs saveOrUpdate(Contacs contacs) {
		return contacsRepository.save(contacs);
	}

	public void remove(Long id) {
		contacsRepository.deleteById(id);
	}
	
	public List <Contacs> getContacts(String username) {
		return contacsRepository.getContacts(username);
	}
}
