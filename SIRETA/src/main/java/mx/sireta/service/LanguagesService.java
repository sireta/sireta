package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Languages;
import mx.sireta.repository.LanguagesRepository;

@Service
public class LanguagesService {

    @Autowired
    private LanguagesRepository languagesRepository;

    public List<Languages> getAll(){
        return languagesRepository.findAll();
    }

    public Languages getOne(Long id){
        return languagesRepository.findById(id).get();
    }

    public Languages saveOrUpdate(Languages language){
        return languagesRepository.save(language);
    }

    public void remove(Long id){
        languagesRepository.deleteById(id);
    }

    public List<Languages> getAllByCurriculum(Long id){
        return languagesRepository.getLanguagesByCurriculum_IdCurriculumGeneral(id);
    }

}
