package mx.sireta.service;

import mx.sireta.entity.Rol;
import mx.sireta.repository.RolRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class RolService {

    @Autowired
    private RolRepository rolRepository;

    public List<Rol> getAll(){
        return rolRepository.findAll();
    }

    public Rol saveOrUpdate(Rol rol) {
        return rolRepository.save(rol);
    }

    public void remove(int id) {
        rolRepository.deleteById(id);
    }

}
