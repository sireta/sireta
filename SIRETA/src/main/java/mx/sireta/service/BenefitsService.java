package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Benefits;
import mx.sireta.repository.BenefitsRepository;

@Service
public class BenefitsService {

    @Autowired
    private BenefitsRepository benefitsRepository;

    public List<Benefits> getAll(){
        return benefitsRepository.findAll();
    }

    public Benefits getOne(Long id){
        return benefitsRepository.findById(id).get();
    }

    public Benefits saveOrUpdate(Benefits benefit){
        return benefitsRepository.save(benefit);
    }

    public void remove(Long id){
        benefitsRepository.deleteById(id);
    }

    public void deleteAllByVacantId(Long idVacant){
        benefitsRepository.deleteAllByVacantId(idVacant);
    }
    
    public List<Benefits> getBenefitsByRecruiter(String username){
    	return benefitsRepository.getBenefitsByRecruiter(username);
    }

    public List<Benefits> getBenefitsByIdVacant(Long idVacant) {
    	return benefitsRepository.getBenefitsByVacantIdVacant(idVacant);
    }
    
    public List<Benefits> getBenefitsByVacant(String username){
    	return benefitsRepository.getBenefitsByVacant(username);
    }
}
