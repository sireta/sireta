package mx.sireta.service;

import mx.sireta.entity.WorkExperiences;
import mx.sireta.repository.WorkExperiencesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class WorkExperiencesService {

    @Autowired
    private WorkExperiencesRepository workExperiencesRepository;

    public List<WorkExperiences> getAll(){
        return workExperiencesRepository.findAll();
    }

    public WorkExperiences getWorkExperiences(Long id){
        return workExperiencesRepository.findById(id).get();
    }

    public WorkExperiences saveOrUpdate(WorkExperiences workExperiences){
        return workExperiencesRepository.save(workExperiences);
    }

    public List<WorkExperiences> getWorkExperienceByCurriculum(Long id){
        return workExperiencesRepository.getWorkExperiencesByCurriculum_IdCurriculumGeneral(id);
    }

    public void remove(Long id){
        workExperiencesRepository.deleteById(id);
    }

}
