package mx.sireta.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.thymeleaf.context.Context;
import org.thymeleaf.spring5.SpringTemplateEngine;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.nio.charset.StandardCharsets;
import java.util.HashMap;
import java.util.Map;

@Service
public class EmailService {

    @Autowired
    private JavaMailSender mailSender;

    @Autowired
    private SpringTemplateEngine templateEngine;

    public void sendTemplateMail(String email, String opcionE, String correo) throws MessagingException {
        MimeMessage message = mailSender.createMimeMessage();
        MimeMessageHelper helper =
                new MimeMessageHelper(message,MimeMessageHelper.MULTIPART_MODE_MIXED_RELATED,
                        StandardCharsets.UTF_8.name());
        Map<String, Object> map = new HashMap<String, Object>();
        map.put("correo",correo); //Mensaje, contenido (correo) Variables
        map.put("mensaje",opcionE); //Encabezado del correo  Variables
        Context context = new Context();
        context.setVariables(map);
        String html = templateEngine.process("correo",context); //Nombre del archivo HTML con el formato del Correo
        helper.setTo(email); //A quien va dirigido el correo
        helper.setText(html,true);
        helper.setSubject(opcionE);//Encabezado del correo
        mailSender.send(message);
    }

}
