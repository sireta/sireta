package mx.sireta.service;

import java.io.IOException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Company;
import mx.sireta.repository.CompanyRepository;
import org.springframework.web.multipart.MultipartFile;

@Service
public class CompanyService {

    @Autowired
	private CompanyRepository companyRepository;

    public List<Company> getAll(){
		return companyRepository.findAll();
	}

	public Company getCompany(Long id) {
	 	return companyRepository.findById(id).get();
	}

	public boolean findByName(String name){
    	return companyRepository.findByName(name) == 0
				? false: true;
	}

	public boolean findByNameExcludeThisId(String name,long id){
    	return companyRepository.findByNameAndIdCompanyNot(name, id ) == 0
				? false: true;
	}

	public void deleteByName(String name){
    	companyRepository.deleteCompanyByName(name);
	}

	public Company saveOrUpdate(String name, String state) {
		Company company = new Company();
		company.setName(name);
		company.setState(state);
		return companyRepository.save(company);
	}

	public Company saveOrUpdate(String name, String state, MultipartFile logo) throws IOException {
		Company company = new Company();
		company.setName(name);
		company.setState(state);
		company.setLogo(logo.getBytes());
		return companyRepository.save(company);
	}

	public Company saveOrUpdate(long id,String name, String state) {
		Company company = companyRepository.findById(id).get();
		company.setName(name);
		company.setState(state);
		company.setLogo(null);
		return companyRepository.save(company);
	}

	public Company saveOrUpdate(long id,String name, String state, MultipartFile logo) throws IOException {
		Company company = companyRepository.findById(id).get();
		company.setName(name);
		company.setState(state);
		company.setLogo(logo.getBytes());
		return companyRepository.save(company);
	}

	public void remove(Long id) {
		companyRepository.deleteById(id);
	}
}
