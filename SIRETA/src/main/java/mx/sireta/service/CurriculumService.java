package mx.sireta.service;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Curriculum;
import mx.sireta.repository.CurriculumRepository;

import javax.swing.text.html.Option;

@Service
public class CurriculumService {

    @Autowired
	private CurriculumRepository curriculumRepository;

    public List<Curriculum> getAll(){
		return curriculumRepository.findAll();
	}

	public Curriculum getCurriculum(Long id) {
	 	return curriculumRepository.findById(id).get();
	}
		
	public Curriculum saveOrUpdate(Curriculum curriculum) {
		return curriculumRepository.save(curriculum);
	}

	public void remove(Long id) {
		curriculumRepository.deleteById(id);
	}

	public Optional<Curriculum> getCurriculumByApplicantsPersons_Id(Long id){
		return curriculumRepository.getCurriculumByApplicants_Persons_Id(id);
	}

}
