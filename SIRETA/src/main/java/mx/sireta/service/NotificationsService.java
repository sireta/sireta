package mx.sireta.service;

import mx.sireta.entity.Notifications;
import mx.sireta.repository.NotificationsRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class NotificationsService {

	@Autowired
	private NotificationsRepository notificationsRepository;

	public List<Notifications> getAll(){
		return notificationsRepository.findAll();
	}

	public Notifications getOne(Long id){
		return notificationsRepository.findById(id).get();
	}

	public Notifications saveOrUpdate(Notifications notifications){
		return notificationsRepository.save(notifications);
	}

	public void remove(Long id){
		notificationsRepository.deleteById(id);
	}

}
