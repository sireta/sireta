package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Course;
import mx.sireta.repository.CourseRepository;

@Service
public class CourseService {

    @Autowired
	private CourseRepository courseRepository;

    public List<Course> getAll(){
		return courseRepository.findAll();
	}

	public Course getCourse(Long id) {
	 	return courseRepository.findById(id).get();
	}
		
	public Course saveOrUpdate(Course course) {
		return courseRepository.save(course);
	}

	public void remove(Long id) {
		courseRepository.deleteById(id);
	}

	public List<Course> getCoursesByCurriculum(Long id){
    	return courseRepository.getCourseByCurriculum_IdCurriculumGeneral(id);
	}

}
