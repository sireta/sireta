package mx.sireta.service;

import mx.sireta.entity.Persons;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import javax.mail.MessagingException;

@Service
public class CorreoService {

    @Autowired
    EmailService emailService;

    @Autowired
    PersonsService personsService;

    public void correo(String correo, String encabezado, String mensaje){
        try {
            emailService.sendTemplateMail(correo,encabezado,mensaje);
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public void recoverPassword(String correo){
        Persons persons =  personsService.getByUsername(correo);
        if(persons.getId() != 0){
            String mensaje = "Tu nueva contraseña es: ";
            String pswd = "Tl";
            for (int i = 0; i < 6; i++) {
                pswd+=((int)(Math.random() * 9));
            }
            pswd += "*";
            mensaje += pswd;
            personsService.updatePasswordByUsername(correo, pswd);
            try {
                emailService.sendTemplateMail(correo,"Recuperación de contraseña",mensaje);
            } catch (MessagingException e) {
                e.printStackTrace();
            }
        }
    }

}
