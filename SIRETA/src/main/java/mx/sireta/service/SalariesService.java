package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Salaries;
import mx.sireta.repository.SalariesRepository;

@Service
public class SalariesService {

    @Autowired
	private SalariesRepository salariesRepository;

    public List<Salaries> getAll(){
		return salariesRepository.findAll();
	}

	public Salaries getSalaries(long id) {
	 	return salariesRepository.findById(id).get();
	}
		
	public Salaries saveOrUpdate(Salaries salaries) {
		return salariesRepository.save(salaries);
	}

	public void remove(Long id) {
		salariesRepository.deleteById(id);
	}
}
