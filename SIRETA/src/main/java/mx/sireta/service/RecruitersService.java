package mx.sireta.service;

import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import mx.sireta.entity.Recruiters;
import mx.sireta.repository.RecruitersRepository;

@Service
public class RecruitersService {
	
	@Autowired
	private RecruitersRepository recruiterRepository;

	public List<Recruiters> getAll(){
		return recruiterRepository.findAll();
	}

	public Recruiters getOne(Long id){
		return recruiterRepository.findById(id).get();
	}

	public Recruiters saveOrUpdate(Recruiters recruiter){
		return recruiterRepository.save(recruiter);
	}

	public void remove(Long id){
		recruiterRepository.deleteById(id);
	}

	public Recruiters getByPersonId(Long id){
        return recruiterRepository.findByPersonId(id);
    }
}
