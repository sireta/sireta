package mx.sireta.service;

import mx.sireta.entity.Applicants;
import mx.sireta.entity.Persons;
import mx.sireta.entity.VacantByApplicant;
import mx.sireta.repository.ApplicantsRepository;
import mx.sireta.repository.PersonsRepository;
import mx.sireta.repository.VacantByApplicantRepository;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class ApplicantsService {

    @Autowired
    private ApplicantsRepository applicantsRepository;
    
    @Autowired
    private VacantByApplicantRepository vacantByApplicantRepository;

    public List<Applicants> getAll(){
        return applicantsRepository.findAll();
    }

    public Applicants getApplicants(Long id){
        return applicantsRepository.findById(id).get();
    }

    public Applicants saveOrUpdate(Applicants applicants){
        return applicantsRepository.saveAndFlush(applicants);
    }

    public void remove(Long id){
        applicantsRepository.deleteById(id);
    }

    public Applicants getApplicantByUserId(Long id){
        return applicantsRepository.findByPersonsId(id);
    }
    
    public List <VacantByApplicant> getApplicantsByRecruiter(String username) {
    	return vacantByApplicantRepository.getApplicantsByRecruiter(username);
    }

    public Applicants getApplicantsByPersons_Id(Long id){
        return applicantsRepository.getApplicantsByPersons_Id(id);
    }

}
