package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Certifications;

import mx.sireta.repository.CertificationsRepository;

@Service
public class CertificationsService {

    @Autowired
    private CertificationsRepository certificationsRepository;
    
    public List<Certifications> getAll(){
        return certificationsRepository.findAll();
    }

    public Certifications getOne(Long id){
        return certificationsRepository.findById(id).get();
    }

    public Certifications saveOrUpdate(Certifications certification){
        return certificationsRepository.save(certification);
    }

    public List<Certifications> getAllByCurriculum(Long id){
        return certificationsRepository.getCertificationsByCurriculum_IdCurriculumGeneral(id);
    }

    public void remove(Long id){
        certificationsRepository.deleteById(id);
    }
}
