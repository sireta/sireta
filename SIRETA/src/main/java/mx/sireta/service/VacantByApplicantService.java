package mx.sireta.service;

import mx.sireta.entity.VacantByApplicant;
import mx.sireta.repository.VacantByApplicantRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
public class VacantByApplicantService {

    @Autowired
    private VacantByApplicantRepository vacantByApplicantRepository;

    public List<VacantByApplicant> getAll(){
        return vacantByApplicantRepository.findAll();
    }

    public VacantByApplicant getVacantByApplicant(Long id){
        return vacantByApplicantRepository.findById(id).get();
    }

    public VacantByApplicant saveOrUpdate(VacantByApplicant vacantByApplicant){
        return vacantByApplicantRepository.saveAndFlush(vacantByApplicant);
    }

    public void remove(Long id){
        vacantByApplicantRepository.deleteById(id);
    }
    
    public List <VacantByApplicant> getApplicantsByVacant(Long id) {
    	return vacantByApplicantRepository.getApplicantsByVacant(id);
    }
    
    public void updateStatus(String status, Long vacante){
        vacantByApplicantRepository.updateStatus(status, vacante);
    }

}
