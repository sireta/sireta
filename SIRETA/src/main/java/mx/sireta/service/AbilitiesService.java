package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Abilities;
import mx.sireta.repository.AbilitiesRepository;

@Service
public class AbilitiesService {

    @Autowired
    private AbilitiesRepository abilitiesRepository;

    public List<Abilities> getAll(){
        return abilitiesRepository.findAll();
    }
    
    public Abilities getOne(Long id){
        return abilitiesRepository.findById(id).get();
    }

    public Abilities saveOrUpdate(Abilities ability){
        return abilitiesRepository.save(ability);
    }

    public void remove(Long id){
        abilitiesRepository.deleteById(id);
    }

    public List<Abilities> getAbilitiesByPersonId(Long id){
        return abilitiesRepository.getAbilitiesByCurriculum_IdCurriculumGeneral(id);
    }

}
