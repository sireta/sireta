package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import mx.sireta.entity.Vacant;
import mx.sireta.repository.BenefitsRepository;
import mx.sireta.repository.VacantRepository;

@Service
public class VacantService {
    
    @Autowired
	private VacantRepository vacantRepository;

    public List<Vacant> getAll(){
		return vacantRepository.findAll();
	}

	public Vacant getVacant(Long id) {
	 	return vacantRepository.findById(id).get();
	}
		
	public Vacant saveOrUpdate(Vacant vacant) {
		return vacantRepository.save(vacant);
	}

	public void remove(Long id) {
		vacantRepository.deleteById(id);
	}
	
	public List<Vacant> getVacantsByRecruiter(String username){
		return vacantRepository.getVacantsByRecruiter(username);
	}
	
	public List<Vacant> getVacantsByState(String username){
		return vacantRepository.getVacantsByState(username);
	}
	
	public List<Vacant> getVacantsByFilter(String username, String filter){
		return vacantRepository.getVacantsByFilter(username, filter);
	}
	
}
