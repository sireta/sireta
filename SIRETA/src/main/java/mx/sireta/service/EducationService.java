package mx.sireta.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import mx.sireta.entity.Education;
import mx.sireta.repository.EducationRepository;

@Service

public class EducationService {

    @Autowired
	private EducationRepository educationRepository;

    public List<Education> getAll(){
		return educationRepository.findAll();
	}

	public Education getEducation(Long id) {
	 	return educationRepository.findById(id).get();
	}
		
	public Education saveOrUpdate(Education education) {
		return educationRepository.save(education);
	}

	public void remove(Long id) {
		educationRepository.deleteById(id);
	}

	public List<Education> getEducationsByCurriculum(Long id){
    	return educationRepository.getEducationByCurriculum_IdCurriculumGeneral(id);
	}

}
