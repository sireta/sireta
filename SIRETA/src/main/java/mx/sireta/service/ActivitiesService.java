package mx.sireta.service;

import mx.sireta.entity.Activities;
import mx.sireta.repository.ActivitiesRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ActivitiesService {

    @Autowired
    private ActivitiesRepository activitiesRepository;

    public List<Activities> getAll(){
        return activitiesRepository.findAll();
    }

    public Activities getActivities(Long id){
        return activitiesRepository.findById(id).get();
    }

    public Activities saveOrUpdate(Activities activities){
        return activitiesRepository.saveAndFlush(activities);
    }

    public void remove(Long id){
        activitiesRepository.deleteById(id);
    }

    public List<Activities> getAbilitiesByCurriculum(Long id){
        return activitiesRepository.getActivitiesByWorkExperiences_IdWorkExperience(id);
    }

}
