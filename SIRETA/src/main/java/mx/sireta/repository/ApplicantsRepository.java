package mx.sireta.repository;

import mx.sireta.entity.Applicants;
import mx.sireta.entity.Persons;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface ApplicantsRepository extends JpaRepository<Applicants, Long>{

    Applicants findByPersonsId(long id);

    Applicants getApplicantsByPersons_Id(Long id);
    
    @Query(value = "SELECT p.* FROM applicants a INNER JOIN persons p ON p.id_person = a.persons_id_persons\r\n"
    		+ "	INNER JOIN vacant_by_applicant va ON a.id_applicant = va.applicant_id_applicant\r\n"
    		+ "    INNER JOIN vacants v ON va.vacant_id_vacant = v.id_vacant\r\n"
    		+ "	INNER JOIN recruiters r ON v.recruiter_id_recruiter = r.id_recruiter\r\n"
    		+ "    INNER JOIN persons pr ON r.person_id_person = pr.id_person\r\n"
    		+ "    WHERE pr.username = :username",nativeQuery = true)
    List <Persons> getApplicantsByRecruiter(@Param("username")String username);
    
}
