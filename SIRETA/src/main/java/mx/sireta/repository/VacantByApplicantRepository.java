package mx.sireta.repository;

import mx.sireta.entity.VacantByApplicant;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface VacantByApplicantRepository extends JpaRepository<VacantByApplicant,Long> {
	
	@Query(value = "SELECT * FROM vacant_by_applicant WHERE vacant_id_vacant = :id;",nativeQuery = true)
    List <VacantByApplicant> getApplicantsByVacant(Long id);
	
	@Query(value = "SELECT * FROM persons p INNER JOIN applicants a ON p.id_person = a.persons_id_persons\r\n"
    		+ "	INNER JOIN vacant_by_applicant va ON a.id_applicant = va.applicant_id_applicant\r\n"
    		+ "    INNER JOIN vacants v ON va.vacant_id_vacant = v.id_vacant\r\n"
    		+ "	INNER JOIN recruiters r ON v.recruiter_id_recruiter = r.id_recruiter\r\n"
    		+ "    INNER JOIN persons pr ON r.person_id_person = pr.id_person\r\n"
    		+ "    WHERE pr.username = :username",nativeQuery = true)
    List <VacantByApplicant> getApplicantsByRecruiter(@Param("username")String username);
	
	@Query(value = "UPDATE vacant_by_applicant SET status = :status WHERE id_vacant_by_applicant = :vacante",nativeQuery = true)
    void updateStatus(@Param("status")String status, @Param("vacante") Long vacante);

}
