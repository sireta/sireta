package mx.sireta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Curriculum;

import java.util.Optional;

@Repository
public interface CurriculumRepository extends JpaRepository<Curriculum, Long> {

    @Query(value = "SELECT c.id_curriculum, c.title, c.applicant_id_applicant FROM curriculum c\n" +
            "INNER JOIN applicants a ON a.id_applicant = c.applicant_id_applicant\n" +
            "INNER JOIN persons p ON p.id_person = a.persons_id_persons\n" +
            "WHERE p.id_person =:id", nativeQuery = true)
    Optional<Curriculum> getCurriculumByApplicants_Persons_Id(Long id);

}
