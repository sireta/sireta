package mx.sireta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Abilities;

import java.util.List;

@Repository
public interface AbilitiesRepository extends JpaRepository<Abilities, Long>{

    List<Abilities> getAbilitiesByCurriculum_IdCurriculumGeneral(Long id);

}
