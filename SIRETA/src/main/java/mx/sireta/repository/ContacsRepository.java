package mx.sireta.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Contacs;

@Repository
public interface ContacsRepository extends JpaRepository<Contacs, Long> {
	
	@Query(value = "SELECT * FROM persons p \r\n"
			+ "	INNER JOIN contacs c ON p.id_person = c.person_main\r\n"
			+ " INNER JOIN persons pf ON c.person_friend = pf.id_person\r\n"
			+ "	WHERE p.username = :username",nativeQuery = true)
    List <Contacs> getContacts(@Param("username") String username);

}

