package mx.sireta.repository;

import mx.sireta.entity.WorkExperiences;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface WorkExperiencesRepository extends JpaRepository<WorkExperiences, Long> {

    List<WorkExperiences> getWorkExperiencesByCurriculum_IdCurriculumGeneral(Long id);

}

