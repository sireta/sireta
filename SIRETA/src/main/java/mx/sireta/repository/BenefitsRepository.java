package mx.sireta.repository;

import java.util.List;

import javax.transaction.Transactional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Benefits;

@Repository
public interface BenefitsRepository extends JpaRepository<Benefits, Long> {
	
	@Query(value = "SELECT b.* FROM benefits b\r\n"
			+ "	INNER JOIN vacants v ON  b.vacant_id_vacant = v.id_vacant\r\n"
			+ " INNER JOIN recruiters r ON v.recruiter_id_recruiter = r.id_recruiter\r\n"
			+ " INNER JOIN persons p ON r.person_id_person = p.id_person\r\n"
			+ " WHERE p.username =  :username",nativeQuery = true)
    List <Benefits> getBenefitsByRecruiter(@Param("username") String username);

	@Query(value = "SELECT * FROM benefits WHERE vacant_id_vacant = :idVacant", nativeQuery = true)
    List<Benefits> getBenefitsByVacantIdVacant(@Param("idVacant") Long idVacant);
	
	@Modifying
    @Transactional
	@Query(value = "DELETE FROM benefits WHERE vacant_id_vacant = :idVacant", nativeQuery = true)
    void deleteAllByVacantId(@Param("idVacant") Long idVacant);
	
	@Query(value = "SELECT b.* FROM benefits b\r\n"
			+ "	INNER JOIN vacants v ON  b.vacant_id_vacant = v.id_vacant\r\n"
			+ " INNER JOIN applicants a ON v.state = a.state\r\n"
			+ " INNER JOIN persons p ON a.persons_id_persons = p.id_person\r\n"
			+ " WHERE p.username =  :username",nativeQuery = true)
    List <Benefits> getBenefitsByVacant(@Param("username") String username);
}
