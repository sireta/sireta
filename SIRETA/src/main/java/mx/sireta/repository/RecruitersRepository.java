package mx.sireta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import mx.sireta.entity.Recruiters;
import org.springframework.stereotype.Repository;

@Repository
public interface RecruitersRepository extends JpaRepository <Recruiters, Long> {
    /**
     * @param id
     * @return
     */
    Recruiters findByPersonId(long id);
}
