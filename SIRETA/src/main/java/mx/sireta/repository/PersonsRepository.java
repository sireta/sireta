package mx.sireta.repository;

import mx.sireta.entity.Persons;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface PersonsRepository extends JpaRepository<Persons, Long> {

    //Búsquedad de usuario por medio del username -> Búsqueda necesaria para el Security
    Persons findPersonsByUsername(String username);

    @Modifying
    @Transactional
    @Query(value = "UPDATE persons SET password =:newPassword WHERE username =:correo", nativeQuery = true)
    void updatePassword(String newPassword, String correo);

    @Query(value = "SELECT * FROM persons p \r\n"
	+ "	WHERE NOT p.username =:username AND p.rol_id_rol = 2",nativeQuery = true)
    List <Persons> getAllPersons(String username);
}
