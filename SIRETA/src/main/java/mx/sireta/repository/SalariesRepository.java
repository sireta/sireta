package mx.sireta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Salaries;

@Repository
public interface SalariesRepository extends JpaRepository<Salaries, Long> {

}
