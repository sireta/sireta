package mx.sireta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Certifications;

import java.util.List;

@Repository
public interface CertificationsRepository extends JpaRepository<Certifications, Long> {

    List<Certifications> getCertificationsByCurriculum_IdCurriculumGeneral(Long id);

}
