package mx.sireta.repository;

import mx.sireta.entity.Rol;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface RolRepository extends JpaRepository<Rol, Integer> {

    @Query(value = "SELECT * FROM rol WHERE nombre=:user",nativeQuery = true)
    Rol getRoles(String user);
}
