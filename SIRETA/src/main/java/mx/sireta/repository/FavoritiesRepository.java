package mx.sireta.repository;

import mx.sireta.entity.Favorities;
import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface FavoritiesRepository extends JpaRepository<Favorities, Long> {
	
}

