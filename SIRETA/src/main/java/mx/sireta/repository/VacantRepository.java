package mx.sireta.repository;

import java.util.List;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Vacant;

@Repository
public interface VacantRepository extends JpaRepository<Vacant, Long> {
	
	@Query(value = "SELECT v.* FROM favorities f INNER JOIN vacants v ON f.vacant_id_vacant = v.id_vacant\r\n"
			+ "	INNER JOIN applicants a ON f.applicant_id_applicant = a.id_applicant\r\n"
			+ " INNER JOIN persons p ON a.persons_id_persons = p.id_person WHERE p.username = :username",nativeQuery = true)
    List <Vacant> getFavoritesVacants(@Param("username") String username);

	@Query(value = "SELECT v.* FROM vacants v \r\n"
			+ "	INNER JOIN recruiters r ON v.recruiter_id_recruiter = r.id_recruiter\r\n"
			+ " INNER JOIN persons p ON r.person_id_person = p.id_person\r\n"
			+ "	WHERE p.username = :username",nativeQuery = true)
    List <Vacant> getVacantsByRecruiter(@Param("username") String username);
	
	@Query(value = "SELECT v.* FROM vacants v \r\n"
			+ "    INNER JOIN applicants a ON v.state = a.state\r\n"
			+ "    INNER JOIN persons p ON a.persons_id_persons = p.id_person\r\n"
			+ "    WHERE v.validity_period >= now() AND p.username = :username",nativeQuery = true)
    List <Vacant> getVacantsByState(@Param("username") String username);
	
	@Query(value = "SELECT v.* FROM vacants v \r\n"
			+ " INNER JOIN applicants a ON v.state = a.state "
			+ " INNER JOIN persons p ON a.persons_id_persons = p.id_person "
			+ " WHERE p.username = :username AND v.validity_period >= now() "
			+ " AND v.modality LIKE :filter OR v.type LIKE :filter ",nativeQuery = true)
    List <Vacant> getVacantsByFilter(@Param("username") String username, @Param("filter") String filter);
	
}