package mx.sireta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import mx.sireta.entity.Notifications;
import org.springframework.stereotype.Repository;

@Repository
public interface NotificationsRepository extends JpaRepository <Notifications, Long> {
	
}
