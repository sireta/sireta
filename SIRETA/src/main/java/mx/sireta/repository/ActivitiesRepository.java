package mx.sireta.repository;

import mx.sireta.entity.Activities;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ActivitiesRepository extends JpaRepository<Activities, Long> {

    List<Activities> getActivitiesByWorkExperiences_IdWorkExperience(Long id);

}
