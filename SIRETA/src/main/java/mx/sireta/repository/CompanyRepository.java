package mx.sireta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Company;
import org.springframework.transaction.annotation.Transactional;

@Repository
public interface CompanyRepository extends JpaRepository<Company, Long> {

    @Query(value = "SELECT COUNT(1) FROM companies WHERE name =:nameToFind", nativeQuery = true)
    int findByName(String nameToFind);

    @Modifying
    @Transactional
    @Query(value = "DELETE FROM companies WHERE name =:name", nativeQuery = true)
    void deleteCompanyByName(String name);
    
    @Query(value = "SELECT COUNT(1) FROM companies WHERE name =:nameToFind and id_company != :idToFind", nativeQuery = true)
    int findByNameAndIdCompanyNot(String nameToFind, long idToFind);
}
