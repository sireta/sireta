package mx.sireta.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import mx.sireta.entity.Languages;

import java.util.List;

@Repository
public interface LanguagesRepository extends JpaRepository<Languages, Long>{

    List<Languages> getLanguagesByCurriculum_IdCurriculumGeneral(Long id);

}
